
dirname = '/home/skowron/BetaBeat_analyses/PS/data/20181024/QKEat100A_qh21_qv2435_delta0.003/mat';

datasets = dir([dirname '/*.PStbt.mat']);


ndatasets = length(datasets);

if (ndatasets == 0)
  fprintf(1,'There is no PStbt data in %s\n',dirname);
  return;
end;

currentFolder = pwd;
cd(dirname);


for i=1:ndatasets
 
    
 fname = datasets(i).name;
 
 datestr = fname(1:8);
 timestr = fname(10:16);
 
 fprintf(1,'File %s taken on %s at %s:%s:%s\n',fname, datestr, timestr(1:2),timestr(3:4),timestr(5:6));
 
 data = load([dirname '/' fname],'data');

 if length(data) < 1
     fprintf(1,'File %s Can not find data in the file',fname);
     continue;
 end

 
 if (i==1)
     if  ~exist( [dirname '/bpmcalib.PS.x.mat'] ,'file' )
         fprintf(1,'Running BPM calibration on the first data set \n');
         [cmd1, cmd2] = getBpmCalibrationPs(data.data,false);
         
         % do linking to the produced file
         system(cmd1);
         system(cmd2);
         
     end
 end
 
 outfname = strrep(fname,'.mat','.txt.sdds');
 saveAsSDDS(data.data.PR_BPM,'PS',[dirname '/' outfname]);
 
end

cd(currentFolder);


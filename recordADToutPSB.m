timestampstring = datestr(now,'yyyymmdd_HHMMSS')
daystring = datestr(now,'yyyymmdd');

for i=1:3
  fprintf(1,'Taking pulse %d\n',i); 
  o=matlabJapc.staticGetSignals('PSB.USER.MD5','BR.SCOPE01.CH01/Acquisition');
  data{i}.hor = o;
  dtoplot_h = o.BR_SCOPE01_CH01.Acquisition.value.value(1,:);
  o=matlabJapc.staticGetSignals('PSB.USER.MD5','BR.SCOPE01.CH02/Acquisition');
  data{i}.ver = o;
  dtoplot_v = o.BR_SCOPE01_CH02.Acquisition.value.value(1,:);

  figure(1)
  plot(dtoplot_h);
  figure(2)
  plot(dtoplot_v);
  
  figure(3)
  plot(dtoplot_h(1,4e4:4e4+1e2));
  figure(4)
  plot(dtoplot_v(1,4e4:4e4+1e2));

  pause(10);
end

fname = [timestampstring 'ADTR3out.mat'];
save(fname,'data');


function s = getModelSpos(machine,bpm)

 fcn_name = sprintf('getModelSpos%s(bpm)', machine);
 s = eval(fcn_name);
 
end
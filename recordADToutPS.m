timestampstring = datestr(now,'yyyymmdd_HHMMSS')
daystring = datestr(now,'yyyymmdd');

for i=1:3
  if (i>1)   
      pause(20);
  end

  fprintf(1,'Taking pulse %d\n',i); 
  o=matlabJapc.staticGetSignals('CPS.USER.MD6','PR.SCOPE71.CH01/Acquisition');
  data{i}.inner = o;
  o=matlabJapc.staticGetSignals('CPS.USER.MD6','PR.SCOPE72.CH01/Acquisition');
  data{i}.top = o;
  o=matlabJapc.staticGetSignals('CPS.USER.MD6','PR.SCOPE72.CH02/Acquisition');
  data{i}.bot = o;
  dtoplot = o.PR_SCOPE72_CH02.Acquisition.value.value(1,:);
  
  o=matlabJapc.staticGetSignals('CPS.USER.MD6','PR.SCOPE71.CH02/Acquisition');
  data{i}.outer = o;
  o2 =o;
  
  
  dtoplot2 = o2.PR_SCOPE71_CH02.Acquisition.value.value(1,:);
  
  figure(1)
  plot(dtoplot);
  figure(2)
  plot(dtoplot2);
  
  figure(3)
  plot(dtoplot(1,4e4:4e4+1e2));
  figure(4)
  plot(dtoplot2(1,4e4:4e4+1e2));
  
  
  
 % hold all;
 % plot(o.BR_SCOPE01_CH01.Acquisition.value.value(2,:));
end
fprintf(1,'Done \n');
fname = ['ADT/' timestampstring '.ADTout.2000mV.mat'];
save(fname,'data');


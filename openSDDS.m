function [sdds] =  openSDDS(filename, binary)
   
if ~exist('binary','var')
        binary = false;
end

lines=cell(0);
% open file
fid=fopen(filename);

% read the file
i=0;
while ~feof(fid)
    i=i+1;
    lines{i} = fgetl(fid);
end
disp([num2str(i), ' lines read from ', filename]);

% close the file
fclose(fid);


sdds.header = {'delete me!'};
%sdds.h = zeros(1,1);
%sdds.v = zeros(1,1);

%sdds.h_bpmname = {'delete me!'};
%sdds.v_bpmname = {'delete me!'};

nh = 0;
nv = 0;
for j=1:numel(lines)
    
    
    readin = lines{j};
    
    % consider only valid lines
    %%% Unfrotunatelly this doesn't always work when loading a
    %%% generic TFS table from file.
    % if isempty(readin) || isempty(regexp(readin, '^(\@| *"| *(0-9)|\*|\$)', 'once'));
    %      continue
    %  end
    
    % discard empty lines
    if isempty(readin)
        continue
    end
    
    
   % fprintf(1,'Line : %s \n',readin);
    
    if (readin(1) == '#')
        sdds.header{end+1} = readin;
        
        continue;
    end
    items = strsplit(readin);
    
    plane = items{1}; % 0 horizotnal; 1 vertical
    bpmname = items{2};
    
    pos = zeros(1,length(items) - 2);
    
    
    
    for i=3:length(items)
        v = sscanf(items{i},'%f');
        if (isempty(v))
            continue
        end
        pos(1,i-2) = v;
        % fprintf(1,'%s %s %d %f \n', plane, bpmname,i-2,pos(1,i-2));
    end   
    
    
    %display('put to container');
    %tic
    if (plane == '0')
        nh  = nh + 1;
        sdds.h(nh).pos = pos;
        sdds.h(nh).bpmname = bpmname;
    else
        nv  = nv + 1;
        sdds.v(nv).pos = pos;
        sdds.v(nv).bpmname = bpmname;
    end
end
   
if length(sdds.header) > 2
    sdds.header = sdds.header(2:end);
elseif length(sdds.header) == 2
    sdds.header = sdds.header{2};
else
    sdds.header = [];
end

end
function [knob] =  coupling(BRHO)


%% incaify Matlab instance
%matlabJapc.staticINCAify('PSB');
matlabJapc.staticRBACAuthenticate();
CYCLE = 'PSB.USER.MD4';

%%

beam_Ek = 1.4;
pmass =        0.938272046 ;
LightSpeed = 2.9979246e+08;

beam_Etot = beam_Ek + pmass;
beam_pc = sqrt(beam_Etot*beam_Etot - pmass*pmass);

BRHO     = beam_pc * 3.3356;

fprintf(1,'BRHO = %f \n', BRHO);
% Gint = 0.05 @ 85 A
QSK_exc_const = (0.05 / 85) * LightSpeed * 1e-6 / beam_pc ;



%%

corrFactor = -1; % strength of the correction; 1: all correction

ringnostr = '3';
filename = 'virgin_1.4GeV/changeparameters_couple.R3.tfs';

skipnlines = 5;
knob = importdata(filename,' ',skipnlines);
plot(knob.data)

%%
%timestampstring = datestr(now,'yyyymmdd_HHMMSS');
%daystring = datestr(now,'yyyymmdd');
%if ( exist(daystring,'dir') == false)
%  mkdir(daystring);
%end  


%%
for i=1:length(knob.data)
    madxvarname = knob.textdata{i + skipnlines};
    fprintf(1,'madxvarname = %s \n',madxvarname);
    
    devname = strrep(madxvarname,'k1lQSK',['BR' ringnostr '.QSK']);
    fprintf(1,'devname = %s \n',devname);
    
    kvalue = knob.data(i);

    
    ivalue = kvalue / QSK_exc_const; 

    fprintf(1,'%s K = %f, I = %f \n',devname,kvalue,ivalue);
    
    signal = [devname  '/REF.TABLE'];
    
    fprintf(1,'%s FullSignal %s \n',devname,signal);
    %continue;
    
    reftable = matlabJapc.staticGetSignal(CYCLE,signal);
    fprintf(1,' RefTable in INI %f \n ', reftable.Y);
    %reftable.Y
    
    for i=1:length(reftable.Y)
        if (abs(reftable.Y(i)) > 1e-9)
            reftable.Y(i) = reftable.Y(i) + ivalue*corrFactor;
        elseif (i>2 && i < 5)
            reftable.Y(i) = reftable.Y(i) + ivalue*corrFactor;
        end
        %reftable.Y(i) = 0
    end
    fprintf(1,' RefTable in NEW %f \n ', reftable.Y);
    
    % matlabJapc.staticSetSignal(CYCLE,[signal '#FUNCTION'],reftable);
    
    reftable = matlabJapc.staticGetSignal(CYCLE,signal);
    fprintf(1,' RefTable in HDW %f \n ', reftable.Y);
    

    fprintf(1,'===================== \n');
    
end






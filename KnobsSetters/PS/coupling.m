function [knob] =  coupling(BRHO)


%% incaify Matlab instance
%matlabJapc.staticINCAify('PS');
matlabJapc.staticRBACAuthenticate();
CYCLE = 'CPS.USER.MD6';

%%

beam_Ek = 1.4;
pmass =        0.938272046 ;
beam_Etot = beam_Ek + pmass;
beam_pc = sqrt(beam_Etot*beam_Etot - pmass*pmass);

BRHO     = beam_pc * 3.3356;

fprintf(1,'BRHO = %f \n', BRHO);
KQS_exc_const = 3800.E-6/(BRHO);

%%

corrFactor = -1; % strength of the correction; 1: all correction

%filename = 'TOF_dx6_dy5/changeparameters_couple.tfs';
filename = 'LHClike_KQSoff/changeparameters_couple.tfs';
filename = '/user/cpsop/BetaBeat.out/2018-07-05/PS/Results/Coupling_iter1_v1/changeparameters_couple.tfs';
filename = '/user/cpsop/BetaBeat.out/2018-07-05/PS/Results/Coupling_Iter3_v1/changeparameters_couple.tfs';
filename = '/user/cpsop/BetaBeat.out/2018-07-05/PS/Results/Qh215_Qv23_v1/changeparameters_couple.tfs';

skipnlines = 5;
knob = importdata(filename,' ',skipnlines);
plot(knob.data)

%%
timestampstring = datestr(now,'yyyymmdd_HHMMSS');
daystring = datestr(now,'yyyymmdd');
if ( exist(daystring,'dir') == false)
  mkdir(daystring);
end  


%%
for i=1:length(knob.data)
    madxvarname = knob.textdata{i + skipnlines};
    
    devname = strrep(madxvarname,'PR.K','PR.');
    kvalue = knob.data(i);

    
    ivalue = kvalue / KQS_exc_const; 

    fprintf(1,'%s K = %f, I = %f \n',devname,kvalue,ivalue);
    

    %matlabJapc.staticSetSignal(CYCLE,obj.kickerTimeAcq,obj.kickerAcqStruct);
    %signal = [devname  '/REF.CCV.VALUE'];
    signal = [devname  '/REF.TABLE'];
    
    fprintf(1,'%s FullSignal %s \n',devname,signal);
    %continue;
    
    reftable = matlabJapc.staticGetSignal(CYCLE,signal);
    fprintf(1,' RefTable in INI %f \n ', reftable.Y);
    %reftable.Y
    
    for i=1:length(reftable.Y)
        if (abs(reftable.Y(i)) > 1e-9)
            reftable.Y(i) = reftable.Y(i) + ivalue*corrFactor;
        elseif (i>2 && i < 5)
            reftable.Y(i) = reftable.Y(i) + ivalue*corrFactor;
        end
        %reftable.Y(i) = 0
    end
    fprintf(1,' RefTable in NEW %f \n ', reftable.Y);
    
    matlabJapc.staticSetSignal(CYCLE,[signal '#FUNCTION'],reftable);
    
    reftable = matlabJapc.staticGetSignal(CYCLE,signal);
    fprintf(1,' RefTable in HDW %f \n ', reftable.Y);
    

    fprintf(1,'===================== \n');
    
end






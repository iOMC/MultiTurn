function s = getModelSposPSB_R3(bpm)

%fprintf(1,'getModelSposPSB_R3 BPM is <%s>\n',bpm);

bpmnames = { ...
'BR3.BPM1L3', ...
'BR3.BPM2L3', ...
'BR3.BPMT3L1', ...
'BR3.BPM3L3', ...
'BR3.BPM4L3', ...
'BR3.BPM5L3', ...
'BR3.BPM6L3', ...
'BR3.BPM7L2', ...
'BR3.BPM7L3', ...
'BR3.BPM8L3', ...
'BR3.BPM9L3', ...
'BR3.BPM10L3', ...
'BR3.BPM11L3', ...
'BR3.BPM12L3', ...
'BR3.BPM13L3', ...
'BR3.BPM14L3', ...
'BR3.BPM15L3', ...
'BR3.BPM16L3'};

spos = { ...
5.467797 , ...
15.285302 , ...
21.678 , ...
25.102807 , ...
34.920312 , ...
44.737806 , ...
54.555311 , ...
63.37854 , ...
64.372816 , ...
74.190321 , ...
84.007826 , ...
93.82532 , ...
103.642825 , ...
113.46033 , ...
123.277835 , ...
133.095339 , ...
142.912834 , ...
152.730339 };

m = containers.Map(bpmnames,spos);

s = m(bpm);



end
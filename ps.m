close all
clc
clear all

%% incaify Matlab instance
%matlabJapc.staticINCAify('PS');
matlabJapc.staticRBACAuthenticate();


timestampstring = datestr(now,'yyyymmdd_HHMMSS');
daystring = datestr(now,'yyyymmdd');
if ( exist(daystring,'dir') == false)
  mkdir(daystring);
end  

mySelector = 'CPS.USER.MD1';                
%%




takeArchive = 0;
if (takeArchive)
    [ signalsToArchive, signalsToArchiveNoPPM ]  = psSignalsForArchive();
    
    fprintf(1,'There is signalsToArchive %d \n', length(signalsToArchive));
    
    fprintf(1,'The last one %s \n', signalsToArchive{length(signalsToArchive)});

    archiveNoPPM = matlabJapc.staticGetSignals('Selectors.NON_PPM_SELECTOR',signalsToArchiveNoPPM);
    archive      = matlabJapc.staticGetSignals(mySelector,signalsToArchive);

    archiveFileName = [ daystring '/' timestampstring '.mat'];
    save( archiveFileName , 'archive', 'archiveNoPPM' );

    fprintf(1,'Archive %s taken. Saved %d signals and %d NonPPM signals \n', ...
               archiveFileName, length(archive), length(archiveNoPPM));

end

%% snippet to read the archive
%
% for i=1:length(signalsToArchive)
%   fprintf(1,'     %d  %s \n', i, signalsToArchive{i});
%   [ devname, proptagname ]= strtok(signalsToArchive{i},'/');
%   devname = strrep(devname,'.','_');
%   devname = strrep(devname,'-','_');
%   %fprintf(1,'     %d  devname %s \n', i, devname);
%   da = archive.(devname);
%   [propname, tagname] = strtok(proptagname(2:end),'#');
%   propname = strrep(propname,'.','_');
%   propname = strrep(propname,'-','_');
%   %fprintf(1,'     %d  propname %s \n', i, propname);
%   prop = da.(propname);
%   if (length(tagname) % 1)
%     tagname = tagname(2:end); % strip the hash 
%     val = prop.(tagname).value;
%     fprintf(1,'     %d  tagname <%s% value=%f\n', i, tagname, val);
%   end
%   fprintf(1,'     %d   <<<<  \n', i);
% end



signalsToMonitor = {...
                    'PR.BPM/AcquisitionTrajectoryBBB',...
                    'PR.BPM/AcquisitionTrajectoryRaw',...
                    'PR.BPM/ExpertSetting',...
                    'PA.FREV-SD/Samples',...
                    'PA.FREVCL-SD/Samples',...
                    'PA.FREVOL-SD/Samples',...
                    'PR.BCT-ST/Samples',...
                    'PA.BDOT-SA/Samples',...
                    'PR.BMEAS-B-SD/Samples',...
                    'PR.BMEAS-BDOT-SD/Samples',... 
                    'PR.BCT/Intensity',...
                    'PA.GSRPOS/Setting',...
                    'PR.BQS72/Acquisition',...
                    'PR.BQS72/SamplerAcquisition'
                    };


global nPulse
nPulse = 0;

 theMonitor = matlabJapcMonitor(mySelector, ...
    signalsToMonitor, @(data,h)doSavePsTbT(data,h),...
    ['PS turn by turn BPM data dpp  0mm;'  ...
     'H (0.21 - 0.005) * 436.65 =   89.5 kHz amp 2.5 V' ... 
     'V (0.236 + 0.004) * 436.65 = 104.8 kHz amp 2.5 V']);
 
  

% theMonitor.verbose=true;

theMonitor.saveDataPath = fullfile(['./' daystring] );
theMonitor.saveData = false;
theMonitor.isTryingToGetValues=1;
theMonitor.useFastStrategy(true);

%% start
theMonitor.start(1)


return;


   
                


%%
theMonitor.saveData = true;
%%
theMonitor.saveData = false;
%%
theMonitor.stop



%%errorcode: 
%  0  no error
%  1  warning
%  2  error
% errorcodestr: string with expplanation


function [errorcode,errorcodestr] =  saveAsSDDS(bpmdata, machine, fname, binary)
    useMeanSigma = false;
    useLinFitSigma = true;
    
    if ~exist('binary','var')
        binary = false;
    end
    
    errorcode = 0;% no error;
    errorcodestr = ''; % no comment
    
    v = bpmdata.AcquisitionTrajectoryRaw.value;
    
    expertSettingExternal = 0;
    if (expertSettingExternal)
        %esfn = '/user/psbop/MultiTurn/RefData20180607.mat';
        esfn = '/user/psbop/MultiTurn/20180723/1.4GeV_R3/Qh204_Qv234_Qdh355.5kHz_Qdv411kHz/mat/20180723_144810.PSB_R3.tbt.mat';
        warning(['Using ExpertSetting from file ' esfn])
        whatever = load(esfn,'data');
        expsetting = whatever.data.BR3_BPM.ExpertSetting.value;
    else
       fprintf(1,'Getting expert settings from bpmdata.ExpertSetting.value ... \n');
       expsetting = bpmdata.ExpertSetting.value;
       expsetting
       fprintf(1,'Reading expert settings ... Done \n');
    end
    
    fprintf(1,'Getting channel names ... \n');
    channelNames = bpmdata.AcquisitionTrajectoryBBB.value.channelNames;
    nchannels = length(channelNames);
    nbpms = nchannels/2; % all bpms are dual channel, H and V
    fprintf(1,'Getting channel names ... There is %d BPMs \n', nbpms);

    [sigma_nrow, sigma_ncol] = size(v.sigma);
    fprintf(1,'Size of RAW sigma %d %d \n', sigma_nrow, sigma_ncol);

    nturns = v.nbOfMeasRaw;
    
    nturnsInArr =  int16(length(v.sigma) / nchannels );
    %nbpms = int16(length(v.sigma) / nturns);

    fprintf(1,'No of turns in Raw Data array length %d \n', nturnsInArr);

    
    if (nturnsInArr > nturns)
       ndatapoints = nchannels*nturns;
       
       v.sigma(ndatapoints+1:end)  = [];
       v.deltax(ndatapoints+1:end) = [];
       v.deltay(ndatapoints+1:end) = [];
    
    end
    
    % The data is duplicated 
    
    sigmai = reshape(v.sigma,  [nturns, nchannels]);
    deltax = reshape(v.deltax, [nturns, nchannels]);
    deltay = reshape(v.deltay, [nturns, nchannels]);
    
    % remove duplicate data if there
    sigmai(:,nbpms+1:nchannels) = [];
    deltax(:,nbpms+1:nchannels) = [];
    deltay(:,nbpms+1:nchannels) = [];
    
    
    int16_max =  2^15-1;
    int16_min = -2^15;
    
    
    sigma = double(sigmai);
    
    overflow = false;
    nooverflow = false;
    
    length(sigmai)
    for j=1:nbpms
        for i=1:nturns
            if (sigmai(i,j) < 0)
                sv = sigma(i,j) - int16_min;
                sigma(i,j) =  int16_max + sv;
                overflow = true;
            else
                nooverflow = false;
            end
        end
            
    end
    
    if (overflow && nooverflow)
        %plot(sigmai_org(:,1))
        %hold on;
        %plot(sigma(:,1))
        errorcode = 2;
        errorcodestr = 'Data contain overflow and not overflow sigma, it needs extra care to recover the data!';
        error(errorcodestr);
    end

    if (overflow)
        errorcode = 1;
        errorcodestr = 'Overflow in sigma!';
        warning(errorcodestr)
    end
    
    
    if (useLinFitSigma)
        turns = (1:nturns)';
        for j=1:nbpms
            [ sigma_coeffs(j).c, S] = polyfit(turns, sigma(:,j), 1);
            fprintf(1,'BPM %d coeffs: sigma = a*t + b = %f * t + %f ; normr = %f \n', j, sigma_coeffs(j).c(1),sigma_coeffs(j).c(2), S.normr);
            
        end
        
        %plot(sigma(:,4));
        %hold on
        %fittedY = polyval(sigma_coeffs(4).c, turns);
        %fittedY = sigma_coeffs(4).c(1) * turns + sigma_coeffs(4).c(2);
        %plot(turns, fittedY, 'r-', 'LineWidth', 3);
        %return
    end
    
    
    
    
    deltaxCal = zeros(nturns,nbpms);
    deltayCal = zeros(nturns,nbpms);
    
    xcalfilename = ['bpmcalib.' machine '.x.mat'];
    
    if (exist(xcalfilename,'file') == 0)
        error('saveAsSDDS: BPM calibration files <%s> is missing. SDDS not saved.',xcalfilename);
    else
        fprintf(1,'Using %s for calibration data\n',xcalfilename);
    end
    

    ycalfilename = ['bpmcalib.' machine '.y.mat'];
    if (exist(ycalfilename,'file') == 0)
        error('saveAsSDDS: BPM calibration files <%s> is missing. SDDS not saved.',ycalfilename);
    end
    
    load(xcalfilename,'mcxa','mcxb');
    load(ycalfilename,'mcya','mcyb');
    
    %%
    for i=1:nbpms
      
      deltaxCal(:,i) = deltax(:,i); % *  calfactors(1,i);
      deltayCal(:,i) = deltay(:,i); % *  calfactors(1,i+nbpms);
    end

    %%
    %sigMean = mean()
    if (useMeanSigma)
        sigMean = mean(sigma(1000:1500,:),1);
        for i=1:nbpms
            %fprintf(1,'BPM %d Sigma %f\n',i,sigMean(1,i));
            deltaxCal(:,i) = deltaxCal(:,i) / sigMean(1,i);
            deltayCal(:,i) = deltayCal(:,i) / sigMean(1,i);
        end
    elseif (useLinFitSigma)
        for i=1:nbpms
            for t=1:nturns
                %fprintf(1,'BPM %d Sigma %f\n',i,sigMean(1,i));
                vs = sigma_coeffs(i).c(1) * t + sigma_coeffs(i).c(2);
                deltaxCal(t,i) = deltaxCal(t,i) / vs;
                deltayCal(t,i) = deltayCal(t,i) / vs;
            end
        end
        
    else
        
        deltaxCal = deltaxCal ./ sigma;
        deltayCal = deltayCal ./ sigma;
    end

    %% Apply calibration decoded with getBpmCalibration.m
     
    for i=1:nbpms
      tmp = channelNames(i);
      xbpmname  = tmp{1};
      tmp = channelNames(i+nbpms);
      ybpmname  = tmp{1};
      calx = mcxa(xbpmname);
      caly = mcya(ybpmname);
      offx = mcxb(xbpmname);
      offy = mcyb(ybpmname);
      
      deltaxCal(:,i) = deltaxCal(:,i) * calx + offx;
      deltayCal(:,i) = deltayCal(:,i) * caly + offy;
    end


    
    file = fopen(fname,'w');
    if (file < 0)
     error('saveAsSDDS: cannot open file %s', fname );
    end
    %% Create SDDS HEADER
    
    ts = bpmdata.AcquisitionTrajectoryRaw.timeStamp;
    t = java.sql.Timestamp(ts/1e6);
    
    if (binary)
        fprintf(file,'SDDS1\n');
        fprintf(file,'!# big-endian\n');
        fprintf(file,'&parameter name=acqStamp, type=double, &end\n');
        fprintf(file,'&parameter name=nbOfCapBunches, type=long, &end\n');
        fprintf(file,'&parameter name=nbOfCapTurns, type=long, &end\n');
        fprintf(file,'&array name=horPositionsConcentratedAndSorted, type=float, &end\n');
        fprintf(file,'&array name=verPositionsConcentratedAndSorted, type=float, &end\n');
        fprintf(file,'&array name=bpmNames, type=string, &end\n');
        fprintf(file,'&data mode=binary, &end\n');
        
        fwrite(file,1234,'int','ieee-be'); 
        
        fwrite(file,ts/1e6,'double','ieee-be');
        fwrite(file,1,'long','ieee-be'); 
        fwrite(file,nturns,'long','ieee-be'); 
        % H array
        fwrite(file,2,'int','ieee-be'); % dimensions
        fwrite(file,nbpms,'int','ieee-be'); % dimensions
        fwrite(file,nturns,'int','ieee-be'); % dimensions
        
        fwrite(file,deltaxCal(:,:),'single','ieee-be');
        
        fwrite(file,deltayCal(:,:),'single','ieee-be');

      for i=1:nbpms
        
        tmp = channelNames(i);
        xbpmname  = tmp{1};
        %bpmname = strrep(xbpmname,'.H','');
        fwrite(file,xbpmname,'char*1','ieee-be');
      end
      
      for i=1:nbpms

        tmp = channelNames(i+nbpms);
        ybpmname  = tmp{1};
        fwrite(file,ybpmname,'char*1','ieee-be');
        
      end      
    else
        
        fprintf(file,'#SDDSASCIIFORMAT v1\n');
        fprintf(file,'#Beam: %s\n',machine);
        
        
        calendar = java.util.Calendar.getInstance();
        calendar.setTime(t);
        
        acqdatestr = sprintf('%d-%02d-%02d at %02d-%02d-%02d', ...
            calendar.get(calendar.YEAR),...
            calendar.get(calendar.MONTH)+1,...
            calendar.get(calendar.DAY_OF_MONTH),...
            calendar.get(calendar.HOUR_OF_DAY),...
            calendar.get(calendar.MINUTE),...
            calendar.get(calendar.SECOND));
        
        fprintf(file,'#Acquisition date: %s ', acqdatestr);
        
        fprintf(file,'By: MultiTurn Matlab script (saveAsSDDS.m)\n');
        
        fprintf(file,'#bunchid :0\n');
        fprintf(file,'#number of turns :%d\n',nturns);
        fprintf(file,'#number of monitors :%d\n',nbpms);
        fprintf(file,'#dpp :0\n');
    end
    %%    
        
      for i=1:nbpms
        
        tmp = channelNames(i);
        xbpmname  = tmp{1};
        tmp = channelNames(i+nbpms);
        ybpmname  = tmp{1};
        
        %fprintf(1,'BPM names X/Y %s %s \n', xbpmname, ybpmname);
        
        bpmname = strrep(xbpmname,'.H','');
        spos = getModelSpos(machine,bpmname);

            
        fprintf(file,'0 %s %f',bpmname,spos);
        fprintf(file,' %f',deltaxCal(:,i));
        fprintf(file,' \n');
        
        fprintf(file,'1 %s %f',bpmname,spos);
        fprintf(file,' %f',deltayCal(:,i));
        fprintf(file,' \n');
            
      end
    
    fclose(file);
   

end


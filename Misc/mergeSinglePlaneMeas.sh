hfiles=`/bin/ls Horizontal_Int_25/Driven_tune_0168/*.sdds`
vfiles=`/bin/ls Vertical_Int_25/Driven_tune_0274/*.sdds`


hfilesa=($hfiles)
vfilesa=($vfiles)

nhf=${#hfilesa[@]}
echo "There is $nhf H files"

for i in `seq 0 $((nhf - 1))`; do
  
   echo $i
   hf=${hfilesa[$i]}
   vf=${vfilesa[$i]}
   echo $i: $hf $vf
   
   ii=$(printf "%0*d\n" 2 $i)
   
   outfname="File${ii}.PSBtbt.txt.sdds"
   echo "Output: ${outfname}"
   
   awk 'NR < 8 '            $hf >  ${outfname}
   awk 'NR > 7 && (NR+1)%2' $hf >> ${outfname}
   awk 'NR > 7 && (NR)%2'   $vf >> ${outfname}
   
done

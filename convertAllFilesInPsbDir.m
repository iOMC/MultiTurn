dirname = './';

datasets = dir([dirname '/*.PSB_R*.tbt.mat']);

ndatasets = length(datasets);
fprintf(1,'There is %d PStbt data sets\n',ndatasets);

if (ndatasets == 0)
  fprintf(1,'There is no PStbt data in %s\n',dirname);
  return;
end


for i=1:ndatasets
 
   fname = datasets(i).name;

   datestr = fname(1:8);
   timestr = fname(10:16);

   fprintf(1,'File %s taken on %s at %s:%s:%s\n',fname, datestr, timestr(1:2),timestr(3:4),timestr(5:6));

   whatever = load([dirname '/' fname],'data');
   data = whatever.data;

   if length(data) < 1
       fprintf(1,'File %s Can not find data in the file',fname);
       continue;
   end

   ring = -1;

   if (isfield(data,'BR1_BPM'))
       ring = 1;
       dataBPM = data.BR1_BPM;
   elseif(isfield(data,'BR2_BPM'))
       ring = 2;
       dataBPM = data.BR2_BPM;
   elseif(isfield(data,'BR3_BPM'))
       ring = 3;
       dataBPM = data.BR3_BPM;
   elseif(isfield(data,'BR4_BPM'))
       ring = 4;
       dataBPM = data.BR4_BPM;
   end

%   ring = 3;
%   dataBPM = data.BR3_BPM;

   if (ring < 0)
     warning('doSavePsbTbT: can not find any ring BPM data');
     return;
   else
     fprintf(1,'Found data for PSB Ring %d\n',ring);
   end

   
   machine = sprintf('PSB_R%d',ring);

   calibfilex = [ dirname 'bpmcalib.' machine '.x.mat'];
   
   if  ~exist( calibfilex ,'file' )
       fprintf(1,'Running BPM calibration on the first data set \n');
       [cmd1, cmd2] = getBpmCalibrationPsb(data,false);
       
       % do linking to the produced file
       system(cmd1);
       system(cmd2);
       
   end
   
   
   outfname = strrep(fname,'.mat','.txt.sdds');
   fprintf(1,'\n Saving %s \n ', outfname);
  
   [result(i).error, result(i).errorstr] = saveAsSDDS(dataBPM,machine,[dirname '/' outfname]);
   
   %return;
   
end

for i=1:ndatasets
    fprintf(1,'Result code = %d ; Description: %s \n',result(i).error,result(i).errorstr);
end
function [] = orders(n)

for i=0:n
 fprintf(1,'\n');
 fprintf(1,'x  y \n');
 fprintf(1,'%d  %d ',n-i,i);
% fprintf(1,'x^%d*y^%d ',n-i,i);
 
 if (mod(i,2))
    fprintf(1,'skew \n');
    

    for l=i:-1:0
      m=i-l;  
      for j=n-i:-1:0
          
         if (j==0 && l==0) continue; end
         
         k=(n-i)-j;
         
         fprintf(1,'   res(%2d,%2d) ',j-k,l-m);
         
         fprintf(1,'\t %d %d %d %d ',j,k,l,m);
         if (j ~= 0)
            fprintf(1,'\t H(%2d, %2d) = %2d*Qx + %2d*Qy ',1-j+k,l-m , 1-j+k,l-m );
         else
            fprintf(1,'\t H no line (j=0)             ');
         end
         if (l ~= 0)
           fprintf(1,'\t V(%2d, %2d) = %2d*Qx + %2d*Qy ',k-j,1-l+m , k-j,1-l+m);
         else
           fprintf(1,'\t V no line (l=0)              ');  
         end
         fprintf(1,'\n');
      end
    end
    
    
 else
    fprintf(1,'normal j+k=%d l+m=%d\n',n-i,i);
    %fprintf(1,'Kick H x^%d*y^%d\n ',n-i-1,i);
    %fprintf(1,'Kick V x^%d*y^%d\n ',n-i,i-1);

    for l=i:-1:0
      m=i-l;  
      for j=n-i:-1:0
          
         if (j==0 && l==0) continue; end
          
         k=(n-i)-j;

         fprintf(1,'   res(%2d,%2d) ',j-k,l-m);

         fprintf(1,'\t %d %d %d %d ',j,k,l,m);
         if (j ~= 0)
            fprintf(1,'\t H(%2d, %2d) = %2d*Qx + %2d*Qy ',1-j+k,l-m , 1-j+k,l-m );
         else
            fprintf(1,'\t H no line (j=0)             ');
         end
         if (l ~= 0)
           fprintf(1,'\t V(%2d, %2d) = %2d*Qx + %2d*Qy ',k-j,1-l+m , k-j,1-l+m);
         else
           fprintf(1,'\t V no line (l=0)              ');  
         end
         fprintf(1,'\n');
      end
    end
    
 end
 
end
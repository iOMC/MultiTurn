% addpath  /home/skowron/cern/madx2Matlab/

close all;
clear all;

zoom = 0;

nh = 4000;
hbinsize = 1/nh;
htmp = -0.5:hbinsize:0.5;

hmi = htmp + hbinsize/2;
hmi(end) = [];

hlo = htmp;
hlo(end) = [];

hup = htmp;
hup(1) = [];

hx = hmi;
hx(:) = 0.0;

hy = hmi;
hy(:) = 0.0;

%hmi
%hlo
%hup

dirs = {
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192358_600_4000_f1'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192427_600_4000_f2'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192525_600_4000_f3'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192554_600_4000_f4'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192651_600_4000_f5'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192720_600_4000_f6'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192749_600_4000_f7'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192818_600_4000_f8'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192846_600_4000_f9'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192915_600_4000_f10'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_192944_600_4000_f11'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193013_600_4000_f12'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193042_600_4000_f13'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193110_600_4000_f14'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193139_600_4000_f15'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193208_600_4000_f16'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193237_600_4000_f17'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193306_600_4000_f18'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193334_600_4000_f19'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193403_600_4000_f20'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193432_600_4000_f21'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193501_600_4000_f22'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193530_600_4000_f23'
'/home/skowron/BetaBeat_analyses/PS/temp/2018-06-26/PS/Measurements/20180413_193558_600_4000_f24'
 };


bpms = {'00', '03', '05', '07', '10', '13', '15', '17', '20', '23', '25', '27', '30', '33', '35', '37', '40', ...
        '43', '45', '47', '50', '53', '54', '55', '57', '60', '63', '64', '65', '67', '68', '70', '73', '75', ...
        '77', '80', '83', '85', '87', '90', '93', '95', '97' };

bpms = {'97' };

for bpmidx=1:length(bpms)
bpmno = bpms{bpmidx};

fprintf(1,'bpmidx: %d  %s \n', bpmidx, bpmno);
    
fprintf('READING\n');
for i=1:length(dirs)
  dirname = [dirs{i}];
  
  fname = [dirname '/BPM/' 'PR.BPM' bpmno '.x'];
  fprintf(1,'dir no: %d file X:  %s \n', i, fname);
  data = madx2matlab.parseTFSTableFromFile(fname);
  datatoplot = [];
  datatoplot(1,:) = [data.DATA.FREQ];
  datatoplot(2,:) = [data.DATA.AMP]';

  
  datatoplot = datatoplot';

  freqsorted = sortrows(datatoplot);
  
  for j=length(freqsorted):-1:2
      
    %  fprintf(1,' i=%d  %f %f \n',j, freqsorted(j,1), freqsorted(j-1,1));
      
      if ( abs( freqsorted(j,1) - freqsorted(j-1,1)) < 1e-9 )
         % fprintf(1,' i=%d is the same as the following one %10.8f %10.8f amps %20.18f %20.18f \n',...
         %     j, freqsorted(j,1), freqsorted(j-1,1), freqsorted(j,2), freqsorted(j-1,2));
          
          freqsorted(j-1,2) = freqsorted(j-1,2) + freqsorted(j,2);
          freqsorted(j,:) = [];
      end
  end

  for j=1:length(freqsorted)
      f = freqsorted(j,1);
      a = freqsorted(j,2);
      
      for n=1:length(hmi)
          if ( f>hlo(n) &&  f<hup(n) )
              hx(n) = hx(n) + a;
              break
          end
      end
  end
  
  if (0)
      f3 = figure(3);
      h3 = bar(freqsorted(:,1), freqsorted(:,2),'stacked');
      set(gca,'yscale','log');
      xlim([0.0 0.05]);
      plotfname=[ 'plots/LoFreq.dataset' num2str(i) '.png'];
      print('-dpng',f3,plotfname);
  end
  
 % h1 = plot(freqsorted(:,1), freqsorted(:,2),'s','LineWidth',2);
  figure(1);
  subplot(2,1,1);
  
  h1 = bar(freqsorted(:,1), freqsorted(:,2),'stacked');
  set(gca,'yscale','log');
  if (zoom)
    xlim([0.19 0.28]);
  end
  
  hold all;

  %%  
  subplot(2,1,2);

  
  %fname = [dirname '/getphasetoty_free.out'];
  fname = [dirname '/BPM/' 'PR.BPM00.y'];
  
  fprintf(1,'dir no: %d file Y:  %s \n', i, fname);
  data = madx2matlab.parseTFSTableFromFile(fname);
  datatoplot = [];
  datatoplot(1,:) = [data.DATA.FREQ];
  datatoplot(2,:) = [data.DATA.AMP]';

  
  datatoplot = datatoplot';

  freqsorted = sortrows(datatoplot);
  
  for j=length(freqsorted):-1:2
      
    %  fprintf(1,' i=%d  %f %f \n',j, freqsorted(j,1), freqsorted(j-1,1));
      
      if ( abs( freqsorted(j,1) - freqsorted(j-1,1)) < 1e-9 )
        %  fprintf(1,' i=%d is the same as the following one %10.8f %10.8f amps %20.18f %20.18f \n',...
        %      j, freqsorted(j,1), freqsorted(j-1,1), freqsorted(j,2), freqsorted(j-1,2));
          
          freqsorted(j-1,2) = freqsorted(j-1,2) + freqsorted(j,2);
          freqsorted(j,:) = [];
      end
  end

  for j=1:length(freqsorted)
      f = freqsorted(j,1);
      a = freqsorted(j,2);
      
      for n=1:length(hmi)
          if ( f>hlo(n) &&  f<hup(n) )
              hy(n) = hy(n) + a;
              break
          end
      end
  end

  h2 = bar(freqsorted(:,1), freqsorted(:,2));
  set(gca,'yscale','log');
  if (zoom)  
      xlim([0.19 0.28]); 
  end

  hold all;
  
    
end

end

figure(2)

if (zoom)
  hrangemin =  int16(nh/2 + 0.19*(nh));
  hrangemax =  int16(nh/2 + 0.26*(nh));
else
  hrangemin =  int16(nh/2 + 0.0*(nh)) +1;
  hrangemax =  int16(nh/2 + 0.5*(nh));
end

size(hmi)

subplot(2,1,1);

bar(hmi(hrangemin:hrangemax),hx(hrangemin:hrangemax));
set(gca,'yscale','log');
%xlim([0.19 0.26]);

subplot(2,1,2);

bar(hmi(hrangemin:hrangemax),hy(hrangemin:hrangemax));

set(gca,'yscale','log');


%%
plotRDTlines(.2454,.2804)
subplot(2,1,1);
ylim([1e-3 2e2])
subplot(2,1,2);
ylim([1e-3 2e2])

return;

dirs = {
        '20180809_114628.PStbt.txt'
        '20180809_114824.PStbt.txt'
        '20180809_114908.PStbt.txt'
        '20180809_115037.PStbt.txt'
        '20180809_115122.PStbt.txt'
        '20180809_115206.PStbt.txt'
        '20180809_115251.PStbt.txt'
        '20180809_115335.PStbt.txt'
        '20180809_115419.PStbt.txt'
        '20180809_115504.PStbt.txt'
        '20180809_115548.PStbt.txt'
        '20180809_115632.PStbt.txt'
        '20180809_115717.PStbt.txt'
        '20180809_115801.PStbt.txt'
        '20180809_115846.PStbt.txt'
        '20180809_115930.PStbt.txt'
        '20180809_120014.PStbt.txt'
        '20180809_120143.PStbt.txt'
	 };
Scripts to plot spectra from Sussix and mark RDT lines

First run addSpacesToSyssixFiles.sh
 * Before using matlab script the Sussix BPM files needs to be modified:
    * Add a header line
    * add space at the beginning of each file

function [] = plotRDTlines(qx,qy)

subplot(2,1,1)

[ymima] = ylim();
[xmima] = xlim();

line([qx qx],[ 2e1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text( qx,0.85*ymima(2),'\downarrow Q_x')

line([qy qy],[ 2e1 0.66*ymima(2)],'LineWidth',1,'Color','b','LineStyle',':')
text( qy,0.76*ymima(2),'\downarrow Q_y')

% normal sext
% H( 2, 0) ~x^3
% H(-2, 0) ~x^3
% H( 0, 2) ~xy^2
% H( 0,-2) ~xy^2
% 2Qy - Qx ? in mtrack is there
plotOneRDTline(2*qx      ,0.4, '2Q_x');
plotOneRDTline(2*qy      ,0.4, '2Q_y'); 


%skew sext

plotOneRDTline( qx + qy   ,0.1, 'Q_x + Q_y','-.');
plotOneRDTline( qx - qy   ,0.1, 'Q_x - Q_y','-.'); 

% octupole

plotOneRDTline( 3*qx     ,0.1, '3Q_x'      ,':','m');
plotOneRDTline( qx -2*qy ,0.3, 'Q_x - 2Q_y',':','m');
plotOneRDTline( qx +2*qy ,0.1, 'Q_x + 2Q_y',':','m');


% skew octupole

plotOneRDTline( 2*qx + qy,0.02, '2Q_x + Q_y','-.','m');
plotOneRDTline( 2*qx - qy,0.02, '2Q_x - Q_y','-.','m');
plotOneRDTline( 3*qy     ,0.02 ,'3Q_y'      ,'-.','m');



subplot(2,1,2)

[ymima] = ylim();
line([ qy qy],[ 2e1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qy,0.85*ymima(2),'\downarrow Q_y')

line([ qx qx],[ 2e1 0.66*ymima(2)],'LineWidth',1,'Color','b','LineStyle',':')
text(qx,0.76*ymima(2),'\downarrow Q_x')

% normal sext
% V( 1, 1)
plotOneRDTline(  qy +   qx,0.4,'Q_y + Q_x');
% V(-1, 1)
plotOneRDTline(  qy -   qx,0.4,'Q_y - Q_x');
% V( 1,-1) --> V(-1,1)
% V(-1,-1) --> V( 1,1)

%skew sext

plotOneRDTline(2*qy      ,0.1, '2Q_y','-.');
plotOneRDTline(2*qx      ,0.1, '2Q_x','-.');

% octupole

plotOneRDTline( 2*qx + qy,0.1, '2Q_x + Q_y',':','m');
plotOneRDTline( 2*qx - qy,0.1, '2Q_x - Q_y',':','m');
plotOneRDTline( 3*qy     ,0.1, '3Q_y'      ,':','m');

% skew octupole

plotOneRDTline( 3*qx     ,0.07, '3Q_x'      ,'-.','m');
plotOneRDTline( qx -2*qy ,0.3 , 'Q_x - 2Q_y','-.','m');
plotOneRDTline( qx +2*qy ,0.07, 'Q_x + 2Q_y','-.','m');


end
function [] = plotOneRDTline(qxt,y,name,linestyle,linecolor)

if (nargin < 4)
    linestyle = ':';
end
if (nargin < 5)
    linecolor = 'g';
end

[ymima] = ylim();
[xmima] = xlim();

while(qxt < 0)
    qxt = qxt + 1;
end
while(qxt > 1)
    qxt = qxt - 1;
end

if (qxt > 0.5)
    qxt = 1 - qxt;
end
        

if (qxt > xmima(1) && qxt < xmima(2))
    line([qxt qxt],[ 1e-1 y*ymima(2)],'LineWidth',2,'Color',linecolor,'LineStyle',linestyle)
    text( qxt,(y+0.05)*ymima(2),['\downarrow ' name])
end


end
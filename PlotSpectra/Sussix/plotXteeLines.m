%%

[ymima] = ylim();
[xmima] = xlim();

qdx =  .2056;
qdy =  .2482;

qx = .2094;
qy = .2442;

diffQx = abs(qdx - qx);
diffQy = abs(qdy - qy);

fprintf(1,'diffQx = %f\n',diffQx);
fprintf(1,'diffQy = %f\n',diffQy);
%%

subplot(2,1,1)

line([qdx qdx],[ 2e0 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text( qdx,0.85*ymima(2),'\downarrow Q_{dx}')

line([qx qx],[ 2e0 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text( qx,0.85*ymima(2),'\downarrow Q_x')

line([qdy qdy],[ 2e0 0.66*ymima(2)],'LineWidth',1,'Color','b','LineStyle',':')
text( qdy,0.76*ymima(2),'\downarrow Q_dy')

subplot(2,1,2)

line([qdy qdy],[ 2e0 0.66*ymima(2)],'LineWidth',1,'Color','b','LineStyle',':')
text( qdy,0.76*ymima(2),'\downarrow Q_{dy}')

line([qy qy],[ 2e0 0.66*ymima(2)],'LineWidth',1,'Color','b','LineStyle',':')
text( qy,0.76*ymima(2),'\downarrow Q_y')

line([qdx qdx],[ 2e0 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text( qdx,0.85*ymima(2),'\downarrow Q_{dx}')


%%

subplot(2,1,1)
qv = qdx - diffQx;
str = {'\downarrow Q_dx ','-\DeltaQ'};
line([qv qv],[ 5e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

qv = qdx - 2*diffQx;
str = {'\downarrow Q_dx ','-2\DeltaQ_x'};
line([qv qv],[ 1e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

qv = qdx - 3*diffQx;
str = {'\downarrow Q_dx ','-3\DeltaQ_x'};
line([qv qv],[ 3e-2 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)


qv = qdx + 2*diffQx;
str = {'\downarrow Q_dx ','+2\DeltaQ_x'};
line([qv qv],[ 1e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

qv = qdx + 3*diffQx;
str = {'\downarrow Q_dx ','+3\DeltaQ_x'};
line([qv qv],[ 3e-2 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)


%%

subplot(2,1,2)
qv = qdy + diffQy;
str = {'\downarrow Q_dy ','+\DeltaQ'};
line([qv qv],[ 5e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

qv = qdy + 2*diffQy;
str = {'\downarrow Q_dy ','+2\DeltaQ'};
line([qv qv],[ 5e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

qv = qdy - 2*diffQy;
str = {'\downarrow Q_dy ','-2\DeltaQ'};
line([qv qv],[ 5e-1 0.75*ymima(2)],'LineWidth',2,'Color','r','LineStyle',':')
text(qv, 0.85*ymima(2),str)

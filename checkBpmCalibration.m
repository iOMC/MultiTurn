function [sigma] =  checkBpmCalibration(data)
   useMeanSigma = true;
   
    v = data.PR_BPM.AcquisitionTrajectoryRaw.value;
    expsetting = data.PR_BPM.ExpertSetting.value;
    
    nbpms = length(expsetting.channelNames)/2;
    
    nturns = v.nbOfMeasRaw;
    nbpmsInArr = int16(length(v.sigma) / nturns);
    
    fprintf(1,'No BPMs corresponding to Raw Data array length %d \n', nbpmsInArr);
    fprintf(1,'Real No BPMs %d \n', nbpms);
    fprintf(1,'  Ratio %d \n', nbpmsInArr/nbpms);
    
    
    % The data is duplicated 
    
    % The 
    sigmai = reshape(v.sigma,  [nturns, nbpmsInArr]);
    deltax = reshape(v.deltax,[nturns, nbpmsInArr]);
    deltay = reshape(v.deltay,[nturns, nbpmsInArr]);
    
    % remove duplicate data if there
    sigmai(:,nbpms+1:nbpmsInArr) = [];
    deltax(:,nbpms+1:nbpmsInArr) = [];
    deltay(:,nbpms+1:nbpmsInArr) = [];
    
    
    sigma = double(sigmai);

    gainstr = data.PR_BPM.ExpertSetting.value.gain;
    
    gainidx = psGainstrToIdx(gainstr);

    calfactors = double(data.PR_BPM.ExpertSetting.value.scalingFactor(gainidx,:));
    
    plot(sigma(:,1))

    
    deltaxCal = zeros(nturns,nbpms);
    deltayCal = zeros(nturns,nbpms);
    
    load('bpmcalib.x.mat','mcxa','mcxb');
    load('bpmcalib.y.mat','mcya','mcyb');
    
    %%
    for i=1:nbpms
      
      deltaxCal(:,i) = deltax(:,i); % *  calfactors(1,i);
      deltayCal(:,i) = deltay(:,i); % *  calfactors(1,i+nbpms);
    end

    %%
    %sigMean = mean()
    if (useMeanSigma)
        sigMean = mean(sigma(1000:1500,:),1);
        for i=1:nbpms
            deltaxCal(:,i) = deltaxCal(:,i) / sigMean(1,i);
            deltayCal(:,i) = deltayCal(:,i) / sigMean(1,i);
        end
        
    else
        
        deltaxCal = deltaxCal ./ sigma;
        deltayCal = deltayCal ./ sigma;
    end

    %%
        %%
    for i=1:nbpms
      tmp = expsetting.channelNames(i);
      xbpmname  = tmp{1};
      tmp = expsetting.channelNames(i+nbpms);
      ybpmname  = tmp{1};
      calx = mcxa(xbpmname);
      caly = mcya(ybpmname);
      offx = mcxb(xbpmname);
      offy = mcyb(ybpmname);
      
      deltaxCal(:,i) = deltaxCal(:,i) * calx + offx;
      deltayCal(:,i) = deltayCal(:,i) * caly + offy;
    end


    %%
    f1 = fittype('a*x + b');
    
    mcxa = containers.Map();
    mcxb = containers.Map();
    mcya = containers.Map();
    mcyb = containers.Map();
    
%%
    for i=1:nbpms
    %%
      tmp = expsetting.channelNames(i);
      xbpmname  = tmp{1};
      tmp = expsetting.channelNames(i+nbpms);
      ybpmname  = tmp{1};
      
    %%
      fh = figure(1);
      subplot(2,1,1)
      posdblx = double(data.PR_BPM.AcquisitionTrajectoryBBB.value.position(i,:)) * 0.1; % in mm
      plot(posdblx);
      title(expsetting.channelNames(i));
      ylabel('mm');
      

      subplot(2,1,2)
      posdbly = double(data.PR_BPM.AcquisitionTrajectoryBBB.value.position(i+nbpms,:)) * 0.1; % in mm
      plot(posdbly);
      title(expsetting.channelNames(i+nbpms));
      ylabel('mm');

      tmp = strrep(expsetting.channelNames(i),'.H','.BBB.png');
      plotfname = tmp{1};
      
  
      print(plotfname,'-dpng','-r100');

     % print(fh,figfname, '-dpng','-r100'); 
  
     %%
      f4 = figure(4);
      subplot(2,1,1)
      plot(deltaxCal(:,i));
      title(expsetting.channelNames(i));
      ylabel('\Delta_{X} / \Sigma [ADC counts]');

      subplot(2,1,2)
      plot(deltayCal(:,i));
      title(expsetting.channelNames(i+nbpms));
      ylabel('\Delta_{Y} / \Sigma [ADC counts]');

      
      print('-dpng',f4,strrep(plotfname,'BBB','RAWdivSUM')); 

%%      
      f2 = figure(2);
      subplot(3,1,1)
      plot(deltax(:,i));
      title(expsetting.channelNames(i));
      %ylabel('mm');

      subplot(3,1,2)
      plot(deltay(:,i));
      title(expsetting.channelNames(i+nbpms));
      %ylabel('mm');

      subplot(3,1,3)
      plot(sigma(:,i));
      title(strrep( expsetting.channelNames(i),'.H','.SUM'));
      %ylabel('mm');
      
      plotfname = strrep(plotfname,'BBB','RAW');
      print('-dpng',f2,plotfname); 

      %%
      
      f3 = figure(3);
      clf(f3);
      f3.PaperUnits = 'points';
      f3.PaperPosition = [0 0 400 600];
      set(gcf,'units','points','position',[1800,300,400,600])
      
      subplot(2,1,1)
      hold off;
      plot(deltaxCal(:,i),posdblx,'.k');
      title(expsetting.channelNames(i));
      hold all;
      [cx,gof1] = fit( double(deltaxCal(:,i)),posdblx', f1,'Startpoint',[1 0]);
      
      mcxa(xbpmname) = cx.a;
      mcxb(xbpmname) = cx.b;

      plot(cx,'r');
      legenda_ = legend();
      legenda = legenda_.String;
      legenda{1} = 'RAW vs BBB';
      legenda{2} = sprintf('y= %f x + %f',cx.a,cx.b) ;
      legend(legenda,'Location', 'northwest');
      xlabel('\Delta_{X} / \Sigma [ADC counts]');
      ylabel('pos [mm]');

      dim = [.41 .5 .2 .2];
      str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i));
      annx = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
      annx.FontSize = 9;

      
      subplot(2,1,2)
      hold off;
      plot(deltayCal(:,i),posdbly,'.k');
      title(expsetting.channelNames(i+nbpms));
      ylabel('mm');
      hold all;

      [cy,gof1] = fit( double(deltayCal(:,i)),posdbly', f1,'Startpoint',[1 0]);
      mcya(ybpmname) = cy.a;
      mcyb(ybpmname) = cy.b;
      
      plot(cy,'r');
      legenda_ = legend();
      legenda = legenda_.String;
      legenda{1} = 'RAW vs BBB';
      legenda{2} = sprintf('y= %f x + %f',cy.a,cy.b) ;
      legend(legenda,'Location', 'northwest');
      xlabel('\Delta_{Y} / \Sigma [ADC counts]');
      ylabel('pos [mm]');
     
      dim = [.41 .02 .2 .2];
      str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i+nbpms));
      anny = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
      anny.FontSize = 9;
      
      plotfname = strrep(plotfname,'RAW','RAWvsBBB');
      print('-dpng',f3,plotfname); 

      
    %  return;
      
    %  input('Press any key');
      
      
    end 
   
    %keys(mcyb)
    
    values(mcxa)
    values(mcxb)
    values(mcya)
    values(mcyb)

end


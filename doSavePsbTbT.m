function [] =  doSavePsbTbT(data,h)
    
    
    nPulsesToSave = 20 ;
    
    disp('new beam!');
    
   
    
    if (h.recordedCycles >= nPulsesToSave)
      fprintf(1,'\n');
        
      fprintf(1,'Reached max (%d), stopping automatically \n',nPulsesToSave);
      fprintf(1,'\n');
      h.stop;
    end
    
    ring = -1;
    
    if (isfield(data,'BR1_BPM'))
        ring = 1;
        dataBPM = data.BR1_BPM;

    elseif(isfield(data,'BR2_BPM'))
        ring = 2;
        dataBPM = data.BR2_BPM;
    elseif(isfield(data,'BR3_BPM'))
        ring = 3;
        dataBPM = data.BR3_BPM;
    elseif(isfield(data,'BR4_BPM'))
        ring = 4;
        dataBPM = data.BR4_BPM;
    end
    
    if (ring < 0)
      warning('doSavePsbTbT: can not find any ring BPM data');
      return;
    end

    machine = sprintf('PSB_R%d',ring);
        


    datasize = size(data);
    fprintf(1,'Call no. %d : datasize is %d \n', h.recordedCycles, datasize); 
  
    if (isfield(dataBPM,'ExpertSetting') == false)
        warning('Missing ExpertSetting');
        return;
    end
    
    fprintf(1,'ExpertSetting: \n');
    dataBPM.ExpertSetting

    if (isfield(dataBPM,'AcquisitionTrajectoryBBB') == false)
        warning('Missing AcquisitionTrajectoryBBB');
        dataBPM.AcquisitionTrajectoryBBB
        return;
    end
    
    if (isfield(dataBPM.AcquisitionTrajectoryBBB,'value') == false)
        warning('Missing value in AcquisitionTrajectoryBBB');
        dataBPM.AcquisitionTrajectoryBBB
        return;
    end
    
    fprintf(1,'AcquisitionTrajectoryBBB: \n');
    dataBPM.AcquisitionTrajectoryBBB
    
    if (isstruct(dataBPM.AcquisitionTrajectoryBBB.value) == false)
        warning('value is NaN in AcquisitionTrajectoryBBB');
        dataBPM.AcquisitionTrajectoryBBB.value
        return;
    end
    
    if (isfield(dataBPM,'AcquisitionTrajectoryRaw') == false)
        warning('Missing AcquisitionTrajectoryRaw');
        dataBPM.AcquisitionTrajectoryRaw
        return;
    end
    
    if (isfield(dataBPM.AcquisitionTrajectoryRaw,'value') == false)
        warning('Missing value in AcquisitionTrajectoryRaw');
        dataBPM.AcquisitionTrajectoryRaw
        return;
    end
    
    fprintf(1,'AcquisitionTrajectoryRaw: \n');
    dataBPM.AcquisitionTrajectoryRaw
    dataBPM.AcquisitionTrajectoryRaw.value
    
    if (isstruct(dataBPM.AcquisitionTrajectoryRaw.value) == false)
        warning('value is NaN in AcquisitionTrajectoryRaw');
        dataBPM.AcquisitionTrajectoryRaw
        return;
    end

    
    matfname = [ h.saveDataPath '/' datestr(now,'yyyymmdd_HHMMSS') '.' machine '.tbt.mat'] ;
    fprintf(1,'Call no. %d : saving data in %s \n', h.recordedCycles, matfname); 
    save(matfname,'data');
    
    %% Get variables for plotting Print debug 
    
    
    
    v = dataBPM.AcquisitionTrajectoryRaw.value;
    nturns = v.nbOfMeasRaw;
    
    fprintf(1,'Call no. %d : There is %f BPMs \n', h.recordedCycles, length(v.sigma) / nturns); 
    
    nbpms = int16(length(v.sigma) / nturns);
    
    %% get calibs
    global calibDone;

    if (calibDone == false)
      [cmd1, cmd2] = getBpmCalibrationPsb(data,false);
      % do linking to the produced file
      system(cmd1);
      system(cmd2);
      calibDone = true;
    end

    
    sddstxtfname = [ h.saveDataPath '/' datestr(now,'yyyymmdd_HHMMSS') '.' machine '.tbt.txt.sdds'] ;
    try
        saveAsSDDS(dataBPM,machine,sddstxtfname,0); % 0 as the last parameter == text SDDS
    catch ex
        warning('saveAsSDDS save failed %s',ex.message);
    end
    
    
    %sigma = reshape(dataBPM.AcquisitionTrajectoryRaw.value.sigma,  [nturns, nbpms]);
    deltax = reshape(dataBPM.AcquisitionTrajectoryRaw.value.deltax,[nturns, nbpms]);
    deltay = reshape(dataBPM.AcquisitionTrajectoryRaw.value.deltay,[nturns, nbpms]);

    
    figure(1);
    subplot(2,1,1)
    hold off;
    plot(deltax(:,1))
    hold all

    for i=2:nbpms
      if (i > 5 ) 
        break;
      end
      plot(deltax(:,i))
    end
    title('RAW deltaX');
    
    
    subplot(2,1,2)
    hold off;
    plot(deltay(:,1))
    hold all;
    for i=2:nbpms
      if (i > 5 ) 
        break;
      end
      plot(deltay(:,i))
        
    end
    title('RAW deltaY');
    

    fprintf(1,'Call no. %d : There is %f BPMs \n', h.recordedCycles, length(v.sigma) / nturns); 
    
    fftbpmno = 2;
    
    figure(3)
    
    fftx1 = fft(double(deltax(:,fftbpmno)));
    amp1 = abs(fftx1(2:end));
    amp1 = amp1/max(amp1);
    semilogy(amp1);
    
    noiselevel = mean(amp1(4000:4500));
    fprintf(1,'noiselevel = %f \n', noiselevel);
    fprintf(1,'S/N = %f \n', 1/noiselevel);
    

    figure(4)
    
    ffty1 = fft(double(deltay(:,fftbpmno)));
    ampy1 = abs(ffty1(2:end));
    ampy1 = ampy1/max(ampy1);
    semilogy(ampy1);
    
    noiselevely = mean(ampy1(4000:4500));
    fprintf(1,'noiselevel = %f \n', noiselevely);
    fprintf(1,'S/N = %f \n', 1/noiselevely);

    
end


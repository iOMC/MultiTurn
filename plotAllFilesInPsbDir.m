dirname = './';

datasets = dir([dirname '/*.PSB_R*.tbt.mat']);

ndatasets = length(datasets);
fprintf(1,'There is %d PStbt data sets\n',ndatasets);

if (ndatasets == 0)
  fprintf(1,'There is no PStbt data in %s\n',dirname);
  return;
end


for i=1:ndatasets
 
   fname = datasets(i).name;

   datestr = fname(1:8);
   timestr = fname(10:16);

   fprintf(1,'File %s taken on %s at %s:%s:%s\n',fname, datestr, timestr(1:2),timestr(3:4),timestr(5:6));

   whatever = load([dirname '/' fname],'data');
   data = whatever.data;

   if length(data) < 1
       fprintf(1,'File %s Can not find data in the file',fname);
       continue;
   end

   ring = -1;

   if (isfield(data,'BR1_BPM'))
       ring = 1;
       dataBPM = data.BR1_BPM;
   elseif(isfield(data,'BR2_BPM'))
       ring = 2;
       dataBPM = data.BR2_BPM;
   elseif(isfield(data,'BR3_BPM'))
       ring = 3;
       dataBPM = data.BR3_BPM;
   elseif(isfield(data,'BR4_BPM'))
       ring = 4;
       dataBPM = data.BR4_BPM;
   end

%   ring = 3;
%   dataBPM = data.BR3_BPM;

   if (ring < 0)
     warning('doSavePsbTbT: can not find any ring BPM data');
     return;
   else
     fprintf(1,'Found data for PSB Ring %d\n',ring);
   end

   
   machine = sprintf('PSB_R%d',ring);
   
   plot(data.BA3_FREV_SD.Samples.samples.value,'LineWidth',1);
   xlim([850 1310]);
   hold all;
   
  % return;
end

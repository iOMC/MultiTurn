function F = ACosOmegaTplusPhi2(par, data)
 
 %fprintf(1,'%f \n',par(1:6));
 
 a = par(1);  % X radius of an uprighted ellipse (xup cos(phi))
 om = par(2);  % X radius of an uprighted ellipse (xup cos(phi))
 phi = par(3); % Y radius of an uprighted ellipse

 a2 = par(4);  % X radius of an uprighted ellipse (xup cos(phi))
 phi2 = par(5); % Y radius of an uprighted ellipse
 
 F = a*cos(om*data+phi) + a2*cos(2*om*data+phi2) + par(6);
 
 

end
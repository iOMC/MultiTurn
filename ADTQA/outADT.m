firstidx = 3e4;
lasttidx = 7e4;

%load('20180507_180651ADTR4Vout.mat')
%d1 = double(data{2}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%d2 = double(data{1}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%d3 = double(data{3}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;

load('20180507_174859ADTR4Hout.mat')
d1 = double(data{2}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
d2 = double(data{1}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
d3 = double(data{3}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;


%d1 = d1 - mean(d1);
%d2 = d2 - mean(d2);
%d2 = d2 - mean(d3);

%plot(d1.*d2)

np = 1000;
ds1 = d1(1:np);
ds2 = d2(1:np);
ds3 = d3(1:np);
t = 1:np;
t = samplestep * t;

figure(2)

hold off
plot(t,ds1);
hold on

par(1) = 2e4;
par(2) = 2*pi * 165e3; % 218kHz
par(3) = 3.14/2;
par(4) = 0;

lb(1) = 1e3;
lb(2) = 2*pi * 100e4;
lb(3) = 0;
lb(4) = -1e3 ;

ub(1) = 1e5;
ub(2) = 2*pi * 500e4;
ub(3) = 2*pi;
ub(4) = 1e3 ;

lb = [];
ub = [];


myfun=@(x,xdata)(x(4) + x(1)*cos(x(2)*xdata+x(3)));

%plot(t, ACosOmegaTplusPhi(par,t),'-');

options = optimset('TolFun',1e-18,'TolX',1e-14,'MaxFunEvals',2000);% 'Display','none',



[a_d1, ~] = lsqcurvefit(@ACosOmegaTplusPhi, par, t,ds1, lb,ub, options); %lsqcurvefit lsqnonlin
fprintf(1,'Fit 1 %f %f %f %f  \n',a_d1);
[a_d2, ~] = lsqcurvefit(@ACosOmegaTplusPhi, par, t,ds2, lb,ub, options);
fprintf(1,'Fit 2 %f %f %f %f  \n',a_d2);
[a_d3, ~] = lsqcurvefit(@ACosOmegaTplusPhi, par, t,ds3, lb,ub, options);
fprintf(1,'Fit 3 %f %f %f %f  \n',a_d3);
fprintf(1,'\n');
fprintf(1,'Diff 12 %f %f %f %f  \n',a_d1 - a_d2);
fprintf(1,'Diff 13 %f %f %f %f  \n',a_d1 - a_d3);

plot(t, ACosOmegaTplusPhi(a_d1,t),'-');

dd1 = ds1 - ACosOmegaTplusPhi(a_d1,t);

plot(t, dd1,'LineWidth',1);


figure(3)
tf = 1:length(d1);
tf = samplestep * tf;

% copy phase shift
pars = a_d1; pars(3) = a_d1(3); pars(4) = a_d1(4); 
diff1 = d1 - ACosOmegaTplusPhi(pars,tf);
pars = a_d1; pars(3) = a_d2(3); pars(4) = a_d2(4);
diff2 = d2 - ACosOmegaTplusPhi(pars,tf);
pars = a_d1; pars(3) = a_d3(3); pars(4) = a_d3(4);
diff3 = d3 - ACosOmegaTplusPhi(pars,tf);
subplot(3,1,1);
plot(tf,diff1);
subplot(3,1,2);
plot(tf,diff2);
subplot(3,1,3);
plot(tf,diff3);






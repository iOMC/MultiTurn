function F = CosXplusPhi(par, data)
 phi = data(1, :);

 a = par(1);  % X radius of an uprighted ellipse (xup cos(phi))
 phi0 = par(2); % Y radius of an uprighted ellipse
 F = a*cos(phi+phi0);

end
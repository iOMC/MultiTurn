function F = ACosOmegaTplusPhi(par, data)
 
% fprintf(1,'%f %f %f %f\n',par(1:4));
 
 a = par(1);  % X radius of an uprighted ellipse (xup cos(phi))
 om = par(2);  % X radius of an uprighted ellipse (xup cos(phi))
 phi0 = par(3); % Y radius of an uprighted ellipse
 
 F = a*cos(om*data+phi0) + par(4);
 
 

end
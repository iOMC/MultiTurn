%function [freq] = outADTfit(offset)

close all;
clear all;

if exist('offset','var')
    no = offset;
else
    no = 0;
    display(no);
end

%%

firstidx = 3e4;
lasttidx = 7e4;

%load('20180507_180651ADTR4Vout.mat')
%d1 = double(data{2}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%d2 = double(data{1}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%d3 = double(data{3}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
%samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;

% load('20180507_174859ADTR4Hout.mat')
% d1 = double(data{1}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{2}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;

% load('20180507_143304ADTR3Vout.mat')
% d1 = double(data{2}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE01_CH02.Acquisition.value.sampleInterval ;

 load('20180507_144248ADTR3Hout.mat')
 d1 = double(data{2}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
 d2 = double(data{1}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
 d3 = double(data{3}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
 samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;


% load('20180507_165937ADTR2Vout.mat')
% d1 = double(data{1}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{2}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR https://indico.cern.ch/event/726839/_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE06_CH01.Acquisition.value.sampleInterval ;


% load('20180507_173143ADTR2Hout.mat')
% d1 = double(data{2}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE06_CH01.Acquisition.value.sampleInterval ;


% load('20180507_164556ADTR1Vout.mat')
% d1 = double(data{2}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE07_CH02.Acquisition.value.sampleInterval ;


% load('20180507_161829ADTR1Hout.mat')
% d1 = double(data{2}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE07_CH01.Acquisition.value.sampleInterval ;




%d1 = d1 - mean(d1);
%d2 = d2 - mean(d2);    
%d2 = d2 - mean(d3);
%%
tf = 1:length(d1);
tf = samplestep * tf;

figure(1);
subplot(3,1,1);
plot(tf,d1,'LineWidth',1);
subplot(3,1,2);
plot(tf,d2,'LineWidth',1);
subplot(3,1,3);
plot(tf,d3,'LineWidth',1);

%%

%plot(d1.*d2)
np = 1000;
ds1 = d1(no+1:no+np);
ds2 = d2(no+1:no+np);
ds3 = d3(no+1:no+np);
t = 1:np;
t = samplestep * t;


figure(2)

hold off
plot(t,ds1,'LineWidth',2);
hold on

par(1) = 2.5e4;
par(2) = 2*pi * 175e3; % 218kHz
par(3) = -3.14/2;

par(4) = 0;
par(5) = 3.14/2;    


par(6) = 0;

lb(1) = 1e3;
lb(2) = 2*pi * 100e4;
lb(3) = 0;
lb(4) = -1e3 ;

ub(1) = 1e5;
ub(2) = 2*pi * 500e4;
ub(3) = 2*pi;
ub(4) = 1e3 ;

lb = [];
ub = [];


myfun=@(x,xdata)(x(4) + x(1)*cos(x(2)*xdata+x(3)));

%plot(t, ACosOmegaTplusPhi2(par,t),'-');

options = optimset('Display','none','TolFun',1e-18,'TolX',1e-13,'MaxFunEvals',2000);% 


[a_d1, ~] = lsqcurvefit(@ACosOmegaTplusPhi2, par, t,ds1, lb,ub, options); %lsqcurvefit lsqnonlin
[a_d2, ~] = lsqcurvefit(@ACosOmegaTplusPhi2, par, t,ds2, lb,ub, options);
[a_d3, ~] = lsqcurvefit(@ACosOmegaTplusPhi2, par, t,ds3, lb,ub, options);

fprintf(1,'Fit 1 %f %f %f %f %f %f  \n',a_d1);
fprintf(1,'Fit 2 %f %f %f %f %f %f  \n',a_d2);
fprintf(1,'Fit 3 %f %f %f %f %f %f  \n',a_d3);
fprintf(1,'\n');
freq(1) = a_d1(2) /(2*pi);
freq(2) = a_d2(2) /(2*pi);
freq(3) = a_d3(2) /(2*pi);

fprintf(1,'Fit f1 %f   \n',a_d1(2) /(2*pi));
fprintf(1,'Fit f2 %f   \n',a_d2(2) /(2*pi));
fprintf(1,'Fit f3 %f   \n',a_d3(2) /(2*pi));
fprintf(1,'\n');
fprintf(1,'Diff 12 %f %f %f %f %f %f  \n',a_d1 - a_d2);
fprintf(1,'Diff 13 %f %f %f %f %f %f  \n',a_d1 - a_d3);

plot(t, ACosOmegaTplusPhi2(a_d1,t),'-');

dd1 = ds1 - ACosOmegaTplusPhi2(a_d1,t);
plot(t, dd1,'LineWidth',1);

legend({'data','fit','data - fit'});
%% 

% figure(4)
% off = 2e4;
% hold off
% plot(tf(off+1:off+np),d1(off+1:off+np));
% hold on
% plot(tf(off+1:off+np),                    ACosOmegaTplusPhi2(a_d1,tf(off+1:off+np)),'-');
% 
% plot(tf(off+1:off+np), d1(off+1:off+np) - ACosOmegaTplusPhi2(a_d1,tf(off+1:off+np)),'-');


%%

figure(3)

% copy phase shift
pars = a_d1; %  pars(3) = a_d1(3); pars(6) = a_d1(6); pars(5) = a_d1(5); 
diff1 = d1 - ACosOmegaTplusPhi2(pars,tf);
pars = a_d2; % pars(3) = a_d2(3); pars(6) = a_d2(6); pars(5) = a_d2(5); 
diff2 = d2 - ACosOmegaTplusPhi2(pars,tf);
pars = a_d3; % pars(3) = a_d3(3); pars(6) = a_d3(6); pars(5) = a_d3(5); 
diff3 = d3 - ACosOmegaTplusPhi2(pars,tf);
subplot(3,1,1);
plot(tf,diff1,'LineWidth',1);
subplot(3,1,2);
plot(tf,diff2,'LineWidth',1);
subplot(3,1,3);
plot(tf,diff3,'LineWidth',1);

return

%%
foff = 0:1e3:3.7e4;

for i=1:length(foff); 
    freq(i,1:3)=outADTfit( foff(i) );
    fprintf(1,'%d %f %f %f \n',foff(i),freq(i,1:3));
end
%%
figure(5)
plot(freq(:,1));
hold on
plot(freq(:,2));
plot(freq(:,3));



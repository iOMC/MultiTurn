
close all
clear all

firstidx = 3e4;
lasttidx = 7e4-1;

% load('20180507_180651ADTR4Vout.mat')
% d1 = double(data{2}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE01_CH02.Acquisition.value.sampleInterval ;

% load('20180507_174859ADTR4Hout.mat')
% d1 = double(data{2}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;


% load('20180507_165937ADTR2Vout.mat')
% d1 = double(data{2}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE06_CH01.Acquisition.value.sampleInterval ;


% load('20180507_173143ADTR2Hout.mat')
% d1 = double(data{2}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE06_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE06_CH01.Acquisition.value.sampleInterval ;



% load('20180507_164556ADTR1Vout.mat')
% d1 = double(data{2}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE07_CH02.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE07_CH02.Acquisition.value.sampleInterval ;


% load('20180507_161829ADTR1Hout.mat')
% d1 = double(data{2}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE07_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE07_CH01.Acquisition.value.sampleInterval ;


% load('20180507_144248ADTR3Hout.mat')
% d1 = double(data{2}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d2 = double(data{1}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% d3 = double(data{3}.BR_SCOPE01_CH01.Acquisition.value.value(firstidx:lasttidx));
% samplestep = 1e-9 * data{1}.BR_SCOPE01_CH01.Acquisition.value.sampleInterval ;


load('20180507_143304ADTR3Vout.mat')
d1 = double(data{1}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
d2 = double(data{2}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
d3 = double(data{3}.BR_SCOPE01_CH02.Acquisition.value.value(firstidx:lasttidx));
samplestep = 1e-9 * data{1}.BR_SCOPE01_CH02.Acquisition.value.sampleInterval ;

%d1 = d1 - mean(d1);
%d2 = d2 - mean(d2);
%d2 = d2 - mean(d3);

%plot(d1.*d2)

L = length(d1);

Fs = 1/samplestep;

a1 = fft(d1);
a2 = fft(d2);
a3 = fft(d3);

fs1 = abs(a1/L);
fs2 = abs(a2/L);
fs3 = abs(a3/L);


f2 = Fs*(0:(L-1))/L;
figure(2);
plot(f2,fs1) 

%%
figure(3);
x0=10;
y0=10;
width=550;
height=350;
set(gcf,'units','points','position',[x0,y0,width,height])

f1 = Fs*(0:(L/2))/L;
hs1 = fs1(1:L/2+1);
hs1(2:end-1) = 2*hs1(2:end-1);

hs2 = fs2(1:L/2+1);
hs2(2:end-1) = 2*hs2(2:end-1);

hs3 = fs3(1:L/2+1);
hs3(2:end-1) = 2*hs3(2:end-1);

semilogy(f1,hs1,'LineWidth',1) 
hold on;
semilogy(f1,hs2,'LineWidth',1) 
semilogy(f1,hs3,'LineWidth',1) 

title('ADT spectrum R4 V')
xlabel('frequency [Hz]')
ylabel('amplitude [mV]')



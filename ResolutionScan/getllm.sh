# do GetLLM.py for given range of files having given turn range
# usage:
# getllm.sh  start_turn end_turn  start_file end_file
# input files are by cleanandfft.sh having names like this
#   ${start_turn}_${end_turn}/$fileno

start_turn=$1
end_turn=$2

start_file=$3
end_file=$4

infiles=""

for i in `seq $start_file $end_file`; do

 fileno=$(printf "%0*d\n" 2 $i)
 infname="File$fileno.PSBtbt.txt.sdds"
 
 echo $fileno

 indir="/home/skowron/BetaBeat_analyses/PSB/20180507/scan/r1/Measurements/${start_turn}_${end_turn}/$fileno"
 ls $indir
 
 if [ $i -gt $start_file ]; then
   echo "Adding coma"
   infiles="$infiles,"
 fi
 
 infiles="$infiles${indir}/${infname}"
 

done

outdir="/home/skowron/BetaBeat_analyses/PSB/20180507/scan/r1/Results/${start_turn}_${end_turn}_${start_file}_${end_file}"
#mkdir -p $outdir
echo "======================"
echo  $infiles
echo "======================"
 

 python \
   /home/skowron/cern/Beta-Beat.src/GetLLM/GetLLM.py \
   --accel=PSBOOSTER \
   --model=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/models/PSBOOSTER/ring1/twiss.dat \
   --files=$infiles \
   --output=$outdir \
   --tbtana=SUSSIX \
   --bpmu=mm \
   --nbcpl=0 \
   --lhcphase=0 \
   --errordefs=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/models/PSBOOSTER/ring1/error_deff.txt

echo "outdir $outdir"
# python \
#   /home/skowron/cern/Beta-Beat.src/GetLLM/GetLLM.py \
#   --accel=PSBOOSTER \
#   --model=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/models/PSBOOSTER/ring1/twiss.dat \
#   --files=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File00.PSBtbt.txt/File00.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File01.PSBtbt.txt/File01.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File02.PSBtbt.txt/File02.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File03.PSBtbt.txt/File03.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File05.PSBtbt.txt/File05.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File06.PSBtbt.txt/File06.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File07.PSBtbt.txt/File07.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File08.PSBtbt.txt/File08.PSBtbt.txt.sdds,\
#           /home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/PSBOOSTER/Measurements/File09.PSBtbt.txt/File09.PSBtbt.txt.sdds \
#   --output=/home/skowron/BetaBeat_analyses/PSB/temp/PSBOOSTER/r1/orig \
#   --tbtana=SUSSIX \
#   --bpmu=mm \
#   --nbcpl=0 \
#   --lhcphase=0 \
#   --errordefs=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/models/PSBOOSTER/ring1/error_deff.txt


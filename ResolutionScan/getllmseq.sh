#run getllm.sh in sequence for different files and different turn ranges

startturntot=4000
endturntot=9000


startturn=$startturntot
endturn=$((startturn + 1000))

nfiles=9

while [ $endturn -le $endturntot ] ; do
   echo $startturn $endturn
   
   fromfileno=0
   
   while [ $fromfileno -le $nfiles ] ; do
     
     tofileno=$fromfileno
     
   #  while [ $tofileno -le $nfiles ] ; do
        source getllm.sh $startturn $endturn $fromfileno $tofileno
        tofileno=$((tofileno + 1))
   #  done
     
     fromfileno=$((fromfileno + 1))
     
   done  
   
   #startturn=$((startturn + 1000))
   endturn=$((endturn + 1000))
   
done


# run hole_in_one.py for given sdds input file for range of turns 
# output files are put in 
#   ${start_turn}_${end_turn}/$fileno


start_turn=$1
end_turn=$2
nhf=10


for i in `seq 0 $((nhf - 1))`; do
  fileno=$(printf "%0*d\n" 2 $i)

  infname="File$fileno.PSBtbt.txt.sdds"
  indir="/home/skowron/cern/MultiTurn/data/booster/20180507/RING1/Merged"
  outdir="/home/skowron/BetaBeat_analyses/PSB/20180507/scan/r1/Measurements/${start_turn}_${end_turn}/$fileno"

  mkdir -p $outdir

  echo $indir/$infname
  echo $outdir

    #must have space after wrong_polarity_bpms=
  
  python \
    /home/skowron/cern/Beta-Beat.src/hole_in_one/hole_in_one.py \
    --file=$indir/$infname \
    --model=/home/skowron/BetaBeat_analyses/PSB/temp/2018-05-16/models/PSBOOSTER/ring1/twiss.dat \
    --outputdir=$outdir \
    clean \
    --svd_mode=numpy \
    --startturn=$start_turn  \
    --endturn=$end_turn  \
    --sing_val=8 \
    --pk-2-pk=0.01 \
    --no_exact_zeros \
    --max-peak-cut=20 \
    --single_svd_bpm_threshold=0.97 \
    --bad_bpms= \
    --wrong_polarity_bpms= harpy \
    --harpy_mode=bpm \
    --tunex=0.167 \
    --tuney=0.274 \
    --nattunex=0.170 \
    --nattuney=0.278 \
    --tunez=0.0 \
    --tolerance=0.01 \
    --tune_clean_limit=1e-3 
    
    

done

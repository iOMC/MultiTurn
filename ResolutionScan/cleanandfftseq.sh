#runs cleanandfft.sh sequentailly for different turns
startturntot=4000
endturntot=9000


startturn=$startturntot
endturn=$((startturn + 1000))


while [ $endturn -le $endturntot ] ; do
   echo $startturn $endturn

   source cleanandfft.sh $startturn $endturn
   
   startturn=$((startturn + 1000))
   endturn=$((endturn + 1000))
   
done


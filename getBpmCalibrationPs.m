function [cmd1, cmd2] =  getBpmCalibrationPs(data, doplots)
   if (nargin < 2)
      doplots = true;
      fprintf(1,' getBpmCalibrationPsb defaulting doplots to %d\n',doplots);
   else
      fprintf(1,' getBpmCalibrationPsb doplots is %d\n',doplots);
   end

    v = data.PR_BPM.AcquisitionTrajectoryRaw.value;
    expsetting = data.PR_BPM.ExpertSetting.value;
    
    nchannels = length(expsetting.channelNames);
    nbpms = nchannels/2; % all bpms are dual channel, H and V

    position = data.PR_BPM.AcquisitionTrajectoryBBB.value.position;
    [pos_nrow, pos_ncol] = size(position);
    fprintf(1,'Size of Calibrated position %d %d \n', pos_nrow, pos_ncol);

    [sigma_nrow, sigma_ncol] = size(v.sigma);
    fprintf(1,'Size of RAW sigma %d %d \n', sigma_nrow, sigma_ncol);
    
    
    nturns = v.nbOfMeasRaw;
    
    nturnsInArr =  int16(length(v.sigma) / nchannels );
    %nbpms = int16(length(v.sigma) / nturns);

    fprintf(1,'No of turns in Raw Data array length %d \n', nturnsInArr);

    
    if (nturnsInArr > nturns)
       ndatapoints = nchannels*nturns;
       
       v.sigma(ndatapoints+1:end)  = [];
       v.deltax(ndatapoints+1:end) = [];
       v.deltay(ndatapoints+1:end) = [];
    
    end
    
    
    % The data is duplicated 
    sigmai = reshape(v.sigma,  [nturns, nchannels]);
    deltax = reshape(v.deltax, [nturns, nchannels]);
    deltay = reshape(v.deltay, [nturns, nchannels]);
    
    % remove duplicate data if there
    sigmai(:,nbpms+1:nchannels) = [];
    deltax(:,nbpms+1:nchannels) = [];
    deltay(:,nbpms+1:nchannels) = [];
    
    
    sigma = double(sigmai);

    % Initially it worked like this, gain was a string
    gainstr = data.PR_BPM.ExpertSetting.value.gain;
    fprintf(1,'Gain string is %s \n',gainstr);
    gainidx = psGainstrToIdx(gainstr);
    fprintf(1,'Gain index is %d \n',gainidx);
    
    calfactors = double(data.PR_BPM.ExpertSetting.value.scalingFactor(gainidx,:));

    if (doplots)
      figure(100)
      
      subplot(3,1,1)
      plot(sigma(:,1));

      subplot(3,1,2)
      plot(deltax(:,1));

      subplot(3,1,3)
      plot(deltay(:,1));
      
    end
    
    
    deltaxCal = zeros(nturns,nbpms);
    deltayCal = zeros(nturns,nbpms);
    
    for i=1:nbpms
      deltaxCal(:,i) = deltax(1:nturns,i); % *  calfactors(1,i);
      deltayCal(:,i) = deltay(1:nturns,i); % *  calfactors(1,i+nbpms);
    end

    
    deltaxCal = deltaxCal ./ sigma(1:nturns,:);
    deltayCal = deltayCal ./ sigma(1:nturns,:);
    
    f1 = fittype('a*x + b');
    
    mcxa = containers.Map();
    mcxb = containers.Map();
    mcya = containers.Map();
    mcyb = containers.Map();
%%
    if (doplots)
        if ( exist('calPlots','dir') == false)
            mkdir('calPlots');
        end
    end
    
%%
    for i=1:nbpms
    %%
      tmp = expsetting.channelNames(i);
      xbpmname  = tmp{1};
      tmp = expsetting.channelNames(i+nbpms);
      ybpmname  = tmp{1};
      
      position_unitExponent = data.PR_BPM.AcquisitionTrajectoryBBB.value.position_unitExponent;
      
      
      toMMfactor = power(10,position_unitExponent + 3); % factor to convert in mm
      
      posdblx = double(data.PR_BPM.AcquisitionTrajectoryBBB.value.position(i,:)) * toMMfactor; % in mm
      posdbly = double(data.PR_BPM.AcquisitionTrajectoryBBB.value.position(i+nbpms,:)) * toMMfactor; % in mm
      
      
    if (doplots)
        
        fh = figure(1);
        subplot(2,1,1)
        plot(posdblx);
        title(expsetting.channelNames(i));
        ylabel('mm');
        
        
        subplot(2,1,2)
        plot(posdbly);
        title(expsetting.channelNames(i+nbpms));
        ylabel('mm');
        
        tmp = strrep(expsetting.channelNames(i),'.H','.BBB.png');
        plotfname = tmp{1};
        
        
        print(['calPlots/' plotfname],'-dpng','-r100');
        
        % print(fh,figfname, '-dpng','-r100');
        
        %%
        f4 = figure(4);
        subplot(2,1,1)
        plot(deltaxCal(:,i));
        title(expsetting.channelNames(i));
        ylabel('\Delta_{X} / \Sigma [ADC counts]');
        
        subplot(2,1,2)
        plot(deltayCal(:,i));
        title(expsetting.channelNames(i+nbpms));
        ylabel('\Delta_{Y} / \Sigma [ADC counts]');
        
        
        print('-dpng',f4,['calPlots/' strrep(plotfname,'BBB','RAWdivSUM')]);
        
        %%
        f2 = figure(2);
        subplot(3,1,1)
        plot(deltax(:,i));
        title(expsetting.channelNames(i));
        %xlim([0 15]);
        %ylabel('mm');
        
        subplot(3,1,2)
        plot(deltay(:,i));
        title(expsetting.channelNames(i+nbpms));
        %xlim([0 15]);
        %ylabel('mm');
        
        subplot(3,1,3)
        plot(sigma(:,i));
        title(strrep( expsetting.channelNames(i),'.H','.SUM'));
        %xlim([0 15]);
        %ylabel('mm');
        
        plotfname = strrep(plotfname,'BBB','RAW');
        print('-dpng',f2, ['calPlots/' plotfname]);
        
        %%
        
        f3 = figure(3);
        clf(f3);
        f3.PaperUnits = 'points';
        f3.PaperPosition = [0 0 400 600];
        set(gcf,'units','points','position',[1300,200,400,600])
        
        subplot(2,1,1)
        hold off;
        plot(deltaxCal(:,i),posdblx,'.k');
        title(expsetting.channelNames(i));
        hold all;
        
        %cmd1 = '';cmd2 = '';return;
    end
      
      vx = double(deltaxCal(:,i));
      vy = posdblx';
      vx(~isfinite(vx))=0;
      vy(~isfinite(vy))=0;
      
      try
        [cx,gof1] = fit( vx,vy, f1,'Startpoint',[1 0]);
      
        mcxa(xbpmname) = cx.a;
        mcxb(xbpmname) = cx.b;
        
        if (doplots)
            plot(cx,'r');
            legenda_ = legend();
            legenda = legenda_.String;
            legenda{1} = 'RAW vs BBB';
            legenda{2} = sprintf('y= %f x + %f',cx.a,cx.b) ;
            legend(legenda,'Location', 'northwest');
            xlabel('\Delta_{X} / \Sigma [ADC counts]');
            ylabel('pos [mm]');
            
            dim = [.41 .5 .2 .2];
            str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i));
            annx = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
            annx.FontSize = 9;
        end
        
      catch ex
        warning('Fit for %s failed with exception %s',...
                expsetting.channelNames{i}, ...
                ex.message);
      end

      if (doplots)
          subplot(2,1,2)
          hold off;
          plot(deltayCal(:,i),posdbly,'.k');
          title(expsetting.channelNames(i+nbpms));
          ylabel('mm');
          hold all;
      end

      vx = double(deltayCal(:,i));
      vy = posdbly';
      vx(~isfinite(vx))=0;
      vy(~isfinite(vy))=0;
      
      try
          [cy,gof1] = fit( vx,vy, f1,'Startpoint',[1 0]);
          mcya(ybpmname) = cy.a;
          mcyb(ybpmname) = cy.b;
          %keys(mcyb)
          %values(mcyb)
          
          if (doplots)
              plot(cy,'r');
              legenda_ = legend();
              legenda = legenda_.String;
              legenda{1} = 'RAW vs BBB';
              legenda{2} = sprintf('y= %f x + %f',cy.a,cy.b) ;
              legend(legenda,'Location', 'northwest');
              xlabel('\Delta_{Y} / \Sigma [ADC counts]');
              ylabel('pos [mm]');
              
              dim = [.41 .02 .2 .2];
              str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i+nbpms));
              anny = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
              anny.FontSize = 9;
          end
          
      catch ex
        warning('Fit for %s failed with exception %s',...
                expsetting.channelNames{i+nbpms}, ...
                ex.message);
      end

      if (doplots)
        plotfname = strrep(plotfname,'RAW','RAWvsBBB');
        print('-dpng',f3,['calPlots/' plotfname]); 
      end
      
    %  return;
    %  input('Press any key');
      
      
    end 
    
   %% 
   ts = data.PR_BPM.AcquisitionTrajectoryRaw.timeStamp;
   t = java.sql.Timestamp(ts/1e6);
   calendar = java.util.Calendar.getInstance();
   calendar.setTime(t);
   
   cfname = sprintf('%d%02d%02d_%02d%02d%02d.bpmcalib.mat', ...
    calendar.get(calendar.YEAR),...
    calendar.get(calendar.MONTH)+1,...
    calendar.get(calendar.DAY_OF_MONTH),...
    calendar.get(calendar.HOUR_OF_DAY),...
   	calendar.get(calendar.MINUTE),...
    calendar.get(calendar.SECOND));
    
   save(cfname,'mcxa','mcxb','mcya','mcyb'); 
   
   machine = 'PS';
   xcalfilename = ['bpmcalib.' machine '.x.mat'];
   ycalfilename = ['bpmcalib.' machine '.y.mat'];

   fprintf(1,'Link this files like this to apply in saveAsSDDS.m: \n');
   cmd1 = sprintf('ln -sf %s %s \n',cfname, xcalfilename);
   cmd2 = sprintf('ln -sf %s %s \n',cfname, ycalfilename);
   
   fprintf(1,'    %s \n',cmd1);
   fprintf(1,'    %s \n',cmd2);
   
end


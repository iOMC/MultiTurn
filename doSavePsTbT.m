function [] =  doSavePsTbT(data,h)
    nShotsToRecord = 5;
    disp('new beam!');
    %whos('h')
    
    if (h.recordedCycles == 1)
    %if (0)
      [cmd1, cmd2] = getBpmCalibrationPs(data,false);
      % do linking to the produced file
      system(cmd1);
      system(cmd2);
      
    end
    
    if (h.recordedCycles >= nShotsToRecord)
      fprintf(1,'Reached max (%d), stopping automatically \n',nShotsToRecord);
      h.stop;
    end
    
    datasize = size(data);
    fprintf(1,'Call no. %d : datasize is %d \n', h.recordedCycles, datasize); 
    
    matfname = [ h.saveDataPath '/' datestr(now,'yyyymmdd_HHMMSS') '.PStbt.mat'] ;
    fprintf(1,'Call no. %d : saving data in %s \n', h.recordedCycles, matfname); 
    save(matfname,'data');
    
    sddstxtfname = [ h.saveDataPath '/' datestr(now,'yyyymmdd_HHMMSS') '.PStbt.txt.sdds'] ;
    dataBPM = data.PR_BPM;
   
    saveAsSDDS(dataBPM,'PS',sddstxtfname,0); % 0 as the last parameter == text SDDS
    
    
    v = dataBPM.AcquisitionTrajectoryRaw.value;
    
    nturns = v.nbOfMeasRaw;
    
    fprintf(1,'Call no. %d : There is %f BPMs \n', h.recordedCycles, length(v.sigma) / nturns); 
    
    
    nbpms = int16(length(v.sigma) / nturns);
    
    %sigma = reshape(dataBPM.AcquisitionTrajectoryRaw.value.sigma,  [nturns, nbpms]);
    deltax = reshape(dataBPM.AcquisitionTrajectoryRaw.value.deltax,[nturns, nbpms]);
    deltay = reshape(dataBPM.AcquisitionTrajectoryRaw.value.deltay,[nturns, nbpms]);

    %calPlus = dataBPM.ExpertSetting.value.calibrationFactorPlus;
     
    
    
    %figure(3)
    %plot(dataBPM.AcquisitionTrajectoryRaw.value.deltax)
    
    if (h.recordedCycles > 1)
      close(1);
      close(2);
    end
    
    figure(1);
    plot(deltax(:,1))
    hold all

    for i=2:nbpms
      if (i > 5 ) 
        break;
      end
      plot(deltax(:,i))
    end
    title('RAW deltaX');
    
    
    figure(2);
    plot(deltay(:,1))
    hold all
    for i=2:nbpms
      if (i > 5 ) 
        break;
      end
      plot(deltay(:,i))
    end
    title('RAW deltaY');
    
    if (h.recordedCycles >= nShotsToRecord)
      fprintf(1,'Reached max (%d), stopping automatically \n',nShotsToRecord);
      h.stop;
    end


end


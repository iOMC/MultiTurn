function [cmd1, cmd2] =  getBpmCalibrationPsb(data,doplots)
   
   close all;
   
   if (nargin < 2)
      doplots = true;
      fprintf(1,' getBpmCalibrationPsb: defaulting doplots to %d\n',doplots);
   else
      fprintf(1,' getBpmCalibrationPsb: doplots is %d\n',doplots);
   end
  
   ring = -1;

   if (isfield(data,'BR1_BPM'))
       ring = 1;
       dataBPM = data.BR1_BPM;
   elseif(isfield(data,'BR2_BPM'))
       ring = 2;
       dataBPM = data.BR2_BPM;
   elseif(isfield(data,'BR3_BPM'))
       ring = 3;
       dataBPM = data.BR3_BPM;
   elseif(isfield(data,'BR4_BPM'))
       ring = 4;
       dataBPM = data.BR4_BPM;
   end


   if (ring < 0)
     warning('doSavePsbTbT: can not find any ring BPM data');
     return;
   else
     fprintf(1,' getBpmCalibrationPsb: Found data for PSB Ring %d\n',ring);
   end



    fprintf(1,' getBpmCalibrationPsb: Reading expert settings dataBPM.ExpertSetting \n');
    dataBPM.ExpertSetting
    
    fprintf(1,' getBpmCalibrationPsb: Reading expert settings dataBPM.ExpertSetting.value \n');
    dataBPM.ExpertSetting.value
    
    fprintf(1,' getBpmCalibrationPsb: Reading expert settings ... \n');
    expsetting = dataBPM.ExpertSetting.value;
    
    fprintf(1,' getBpmCalibrationPsb: Reading expert settings ... Done \n');    
    
    
    v = dataBPM.AcquisitionTrajectoryRaw.value;
    
    fprintf(1,' getBpmCalibrationPsb: Getting channel names ... \n');
    
    % originaly it was from expert settings, but sometimes it was missing in
    % the data
    channelNames = dataBPM.AcquisitionTrajectoryBBB.value.channelNames;

    nbpms = length(channelNames)/2;
    fprintf(1,' getBpmCalibrationPsb: Getting channel names ... There is %d BPMs \n', nbpms);
    
    nturns = v.nbOfMeasRaw;
    nbpmsInArr = int16(length(v.sigma) / nturns);
    
    fprintf(1,' getBpmCalibrationPsb: No BPMs corresponding to Raw Data array length %d \n', nbpmsInArr);
    fprintf(1,' getBpmCalibrationPsb:     Real No BPMs %d \n', nbpms);
    fprintf(1,' getBpmCalibrationPsb:     Ratio %d \n', nbpmsInArr/nbpms);
    
    
    % The data is duplicated 
    
    % The 
    sigmai = reshape(v.sigma,  [nturns, nbpmsInArr]);
    deltax = reshape(v.deltax,[nturns, nbpmsInArr]);
    deltay = reshape(v.deltay,[nturns, nbpmsInArr]);
    
    % remove duplicate data if there
    sigmai(:,nbpms+1:nbpmsInArr) = [];
    deltax(:,nbpms+1:nbpmsInArr) = [];
    deltay(:,nbpms+1:nbpmsInArr) = [];
    
    
    sigma = double(sigmai);

    
    % the method is the same as it was for PS
    gainidx = -1
    if 0
        % Initially it worked like this, gain was a string
        gainstr = dataBPM.AcquisitionTrajectoryBBB.value.gain;
        gainstr
        %fprintf(1,'gainstr = %s \n', gainstr);
        gainidx = psGainstrToIdx(gainstr);
    else
        gainidx = dataBPM.AcquisitionTrajectoryBBB.value.gain;
    end
    
    gainidx
    
    
    if (doplots)
      calfactors = double(dataBPM.ExpertSetting.value.scalingFactor(gainidx,:));
      plot(sigma(:,1));
    end
    
    deltaxCal = zeros(nturns,nbpms);
    deltayCal = zeros(nturns,nbpms);
    
    for i=1:nbpms
      deltaxCal(:,i) = deltax(:,i); % *  calfactors(1,i);
      deltayCal(:,i) = deltay(:,i); % *  calfactors(1,i+nbpms);
    end

    
    deltaxCal = deltaxCal ./ sigma;
    deltayCal = deltayCal ./ sigma;
    
    f1 = fittype('a*x + b');
    
    mcxa = containers.Map();
    mcxb = containers.Map();
    mcya = containers.Map();
    mcyb = containers.Map();
  
    if (doplots)
        if ( exist('calPlots','dir') == false)
            mkdir('calPlots');
        end
    end
    
    display('Content of dataBPM.AcquisitionTrajectoryBBB');
    dataBPM.AcquisitionTrajectoryBBB
    display('Content of dataBPM.AcquisitionTrajectoryBBB.value');
    dataBPM.AcquisitionTrajectoryBBB.value
%%
    for i=1:nbpms
    %%
      tmp = channelNames(i);
      xbpmname  = tmp{1};
      tmp = channelNames(i+nbpms);
      ybpmname  = tmp{1};
      posx = dataBPM.AcquisitionTrajectoryBBB.value.position(i,:)';
      posy = dataBPM.AcquisitionTrajectoryBBB.value.position(i+nbpms,:)';
      posdblx = double(posx) * 0.01; % in mm
      posdbly = double(posy) * 0.01; % in mm
      
      % map of overflow values, later removed from the fit
      mapOutOfRangeX = ( posx == -32768);
      mapOutOfRangeY = ( posy == -32768);

      %%
      
      if (doplots)
          
          fh = figure(1);
          subplot(2,1,1)
          plot(posdblx);
          title(['Calibrated BBB' channelNames(i) ] );
          ylabel('mm');
          
          
          subplot(2,1,2)
          plot(posdbly);
          title(channelNames(i+nbpms));
          ylabel('mm');
          
          tmp = strrep(channelNames(i),'.H','.BBB.png');
          plotfname = tmp{1};
          
          print(['calPlots/' plotfname],'-dpng','-r100');
          
          %%
          f4 = figure(4);
          subplot(2,1,1)
          plot(deltaxCal(:,i));
          title(channelNames(i));
          ylabel('\Delta_{X} / \Sigma [ADC counts]');
          
          subplot(2,1,2)
          plot(deltayCal(:,i));
          title(channelNames(i+nbpms));
          ylabel('\Delta_{Y} / \Sigma [ADC counts]');
          
          
          print('-dpng',f4, [ 'calPlots/'  strrep(plotfname,'BBB','RAWdivSUM') ] );
          
          %%
          f2 = figure(2);
          subplot(3,1,1)
          plot(deltax(:,i));
          title(['ADC counts ' channelNames(i)]);
          %xlim([0 15]);
          %ylabel('mm');
          
          subplot(3,1,2)
          plot(deltay(:,i));
          title(channelNames(i+nbpms));
          %xlim([0 15]);
          %ylabel('mm');
          
          subplot(3,1,3)
          plot(sigma(:,i));
          title(strrep( channelNames(i),'.H','.SUM'));
          %xlim([0 15]);
          %ylabel('mm');
          
          plotfname = strrep(plotfname,'BBB','RAW');
          print('-dpng',f2,['calPlots/' plotfname]);
          
          %%
          
          f3 = figure(3);
          clf(f3);
          f3.PaperUnits = 'points';
          f3.PaperPosition = [0 0 400 600];
          set(gcf,'units','points','position',[1300,200,400,600])
          
          subplot(2,1,1)
          hold off;
          plot(deltaxCal(:,i),posdblx,'.b');
          title(channelNames(i));
          hold all;
          
      end
      
      try
        deltaxCalDbl = double(deltaxCal(:,i));
        % remove overflow values
        deltaxCalDbl(mapOutOfRangeX) = 0;
        posdblx(mapOutOfRangeX) = 0;
        % replaces Inf by 0, fit does not like Inf
        deltaxCalDbl(~isfinite(deltaxCalDbl))=0;
        posdblx(~isfinite(posdblx))=0;
         
        [cx,gof1] = fit( deltaxCalDbl ,posdblx, f1,'Startpoint',[1 0]);
      
        mcxa(xbpmname) = cx.a;
        mcxb(xbpmname) = cx.b;
        if (doplots)
  
            plot(deltaxCalDbl,posdblx,'.k');
            
            plot(cx,'r');
            legenda_ = legend();
            legenda = legenda_.String;
            legenda{1} = 'RAW vs BBB';
            legenda{2} = sprintf('y= %f x + %f',cx.a,cx.b) ;
            legend(legenda,'Location', 'northwest');
            xlabel('\Delta_{X} / \Sigma [ADC counts]');
            ylabel('pos [mm]');
            
            dim = [.41 .5 .2 .2];
            str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i));
            annx = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
            annx.FontSize = 9;
        end
      
      catch ex
        warning('Fit for %s failed with exception %s',...
                xbpmname, ...
                ex.message);
      end

      if (doplots)
          
          subplot(2,1,2)
          hold off;
          plot(deltayCal(:,i),posdbly,'.b'); % plot all the values including overflows
          title(channelNames(i+nbpms));
          ylabel('mm');
          hold all;
      end

      try
        deltayCalDbl = double(deltayCal(:,i));
        % remove overflow values
        deltayCalDbl(mapOutOfRangeY) = 0;
        posdbly(mapOutOfRangeY) = 0;
        % replaces Inf by 0, fit does not like Inf
        deltayCalDbl(~isfinite(deltayCalDbl))=0;
        posdbly(~isfinite(posdbly))=0;

        [cy,gof1] = fit( deltayCalDbl,posdbly, f1,'Startpoint',[1 0]);
          mcya(ybpmname) = cy.a;
          mcyb(ybpmname) = cy.b;
          %keys(mcyb)
          %values(mcyb)
 
          if (doplots)
              plot(deltayCalDbl,posdbly,'.k'); % plot with overflows removed

              plot(cy,'r');
              legenda_ = legend();
              legenda = legenda_.String;
              legenda{1} = 'RAW vs BBB';
              legenda{2} = sprintf('y= %f x + %f',cy.a,cy.b) ;
              legend(legenda,'Location', 'northwest');
              xlabel('\Delta_{Y} / \Sigma [ADC counts]');
              ylabel('pos [mm]');
              
              dim = [.41 .02 .2 .2];
              str = sprintf('ExpertSetting#scalingFactor %f',calfactors(1,i+nbpms));
              anny = annotation('textbox',dim,'String',str,'FitBoxToText','on','Color','blue');
              anny.FontSize = 9;
          end
          
      catch ex
        warning('Fit for %s failed with exception %s',...
                ybpmname, ...
                ex.message);
      end

      if (doplots)
          plotfname = strrep(plotfname,'RAW','RAWvsBBB');
          print('-dpng',f3,['calPlots/' plotfname]);
      end
      
    %  return;
    %  input('Press any key');
      
      
    end 
    
   %% 
   ts = dataBPM.AcquisitionTrajectoryRaw.timeStamp;
   t = java.sql.Timestamp(ts/1e6);
   calendar = java.util.Calendar.getInstance();
   calendar.setTime(t);
   
   cfname = sprintf('%d%02d%02d_%02d%02d%02d.bpmcalib.mat', ...
    calendar.get(calendar.YEAR),...
    calendar.get(calendar.MONTH)+1,...
    calendar.get(calendar.DAY_OF_MONTH),...
    calendar.get(calendar.HOUR_OF_DAY),...
   	calendar.get(calendar.MINUTE),...
    calendar.get(calendar.SECOND));
   

   save(cfname,'mcxa','mcxb','mcya','mcyb'); 
   
   xkeys = keys(mcxa);
   ykeys = keys(mcya);
   
   for i=1:length(mcyb)
      xbpmname = xkeys{i};
      ybpmname = ykeys{i};
      fprintf(1,'%s %f %f   %s %f %f \n',...
                 xbpmname,mcxa(xbpmname),mcxb(xbpmname), ...
                 ybpmname,mcya(ybpmname),mcyb(ybpmname));
   end       
   
   machine = sprintf('PSB_R%d',ring);
   xcalfilename = ['bpmcalib.' machine '.x.mat'];
   ycalfilename = ['bpmcalib.' machine '.y.mat'];

   fprintf(1,'Link this files like this to apply in saveAsSDDS.m: \n');
   cmd1 = sprintf('ln -sf %s %s \n',cfname, xcalfilename);
   cmd2 = sprintf('ln -sf %s %s \n',cfname, ycalfilename);
   
   fprintf(1,'    %s \n',cmd1);
   fprintf(1,'    %s \n',cmd2);

end   


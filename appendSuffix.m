function [signals] = appendSuffix(devices,suffix,signals)
  
  lin = length(devices);
  if ( lin == 0) 
    addsigs = {};
    return;
  end
  
  clen = length(signals);
  
  for i=1:length(devices)
    
    signals{1, clen+i} = [devices{i} suffix];
  end
  
  
  
  

import socket

class TG5012A(object):

    def __init__(self, ip_adress, channel=1):
        self.ip = ip_adress
        self.port = 9221 #default port for socket control
        #channel=1 for single PSU and right hand of Dual PSU
        self.channel = channel
        self.ident_string = ''
        self.sock_timeout_secs = 4
        self.packet_end = bytes('\r\n','ascii')
        print('Using port', self.port)
        self.description="Driver for AWG TG5012A"
        self.author="marcel.roger.coly@cern.ch"

    def send(self, cmd):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(self.sock_timeout_secs)
            s.connect((self.ip, self.port))
            s.sendall(bytes(cmd,'ascii'))

    def receive(self, cmd, buffer_size=1024):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(self.sock_timeout_secs)
            s.connect((self.ip, self.port))
            s.sendall(bytes(cmd,'ascii'))
        return s.recv(buffer_size)
            
    def select_channel(self,channel=1):
        '''
        selects channel to be controled (1 or 2)
        '''
        self.send("CHN %d\n" % (channel))

    def get_selected_channel(self):
        '''
        gets selected controled channel (1 or 2)
        '''
        return self.receive("CHN?")
        
    def set_freq(self,freq):
        '''
        sets frequency of selected channel (via select_channel())
        '''
        self.send("FREQ %s\n" % (freq))

    def set_amplitude_unit(self,unit):
        '''
        sets amplitude unit of selected channel (via select_channel())
        unit = 'VPP' | 'VRMS' | 'DBM'
        '''
        self.send("AMPUNIT %s\n" % (unit))

    def set_amplitude(self,amplitude):
        '''
        sets amplitude of selected channel (via select_channel())
        '''
        self.send("AMPL %s\n" % (amplitude))
        
    def set_hi_level(self,level):
        '''
        ##### NOT TESTED #####
        sets high level amplitude of selected channel (via select_channel())
        '''
        self.send("HILVL %s\n" % (level))

    def set_low_level(self,level):
        '''
        ##### NOT TESTED #####
        sets low level amplitude of selected channel (via select_channel())
        '''
        self.send("LOLVL %s\n" % (level))

    def set_dc_offset(self,offset):
        '''
        ##### NOT TESTED #####
        sets dc offset of selected channel (via select_channel())
        '''
        self.send("DCOFFS %s\n" % (offset))

    def set_power_on(self,on='ON'):
        '''
        sets output power ON or OFF
        argument 'on' should be equal to 'ON' or 'OFF'
        '''
        self.send("OUTPUT %s\n" % (on))
        
    def set_pulse_period(self,width):
        '''
        ##### ARB MODE #####
        sets the pulse period in seconds
        '''
        self.send("PER %s\n" % (width))

    def set_rise_time(self,time):
        '''
        ##### NOT TESTED #####
        sets the rise time of the burst in seconds 
        '''
        self.send("PULSRISE %s\n" % (time))

    def set_fall_time(self,time):
        '''
        ##### NOT TESTED #####
        sets the fall time of the burst in seconds 
        '''
        self.send("PULSFALL %s\n" % (time))

    def set_pulse_delay(self,delay):
        '''
        ##### NOT TESTED #####
        sets busrt delay in seconds 
        '''
        self.send("PULSDLY %s\n" % (delay))



qh = 0.21
qv = 0.245
frev = 436630
dfh = 0.001
dfv = 0.001

if (qh < qv):
  freqh = frev*(qh-dfh)
  freqv = frev*(qv+dfv)
else:
  freqh = frev*(qh+dfh)
  freqv = frev*(qv-dfv)

isH = 0

print ("Hello")

if isH:
  print("Using vertical")
  hwfg = TG5012A('172.18.225.179')
  hwfg.set_freq(freqh)
  hwfg.set_amplitude(1.)
else:
  print("Using vertical")
  vwfg = TG5012A('128.141.184.200')
  vwfg.set_freq(freqv)
  vwfg.set_amplitude(1.)



%% clc%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% clc
%% incaify Matlab instance
%matlabJapc.staticINCAify('PSB');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% do rbac authentication
%matlabJapc.staticRBACAuthenticate('psbop');
%matlabJapc.staticRBACAuthenticate();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% get rbac tocken (just to see that it works)
matlabJapc.staticRBACGetToken();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 

timestampstring = datestr(now,'yyyymmdd_HHMMSS');
daystring = datestr(now,'yyyymmdd');
if ( exist(daystring,'dir') == false)
  mkdir(daystring);
end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
mySelector = 'PSB.USER.MD6';

ring = '1';
ring2 = '1';

disp(['ring' ring ', MD user = ' mySelector]);

comment='';
signalsToMonitor = {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
'BR.MPS-SA/Samples#samples' ,...        % main current
'BR.QFO-SA/Samples#samples',...         % QF current
'BR.QDE-SA/Samples#samples',...  
['BA' ring '.FREV-SD/Samples#samples'],...      % new rev freq signal
['BR' ring '.BCT-ST/Samples#samples'],...        % beam intensity
['BR' ring '.QCF-SA/Samples#samples'],...         % F q-strip current
['BR' ring '.QCD-SA/Samples#samples'],...         % D q-strip current
['BR' ring '.BQ-H-ST/Samples#samples'],...        % meas Qx
['BR' ring '.BQ-V-ST/Samples#samples'],...         % meas Qy
['BR' ring2 '.BQ-H-ST/Samples#samples'],...        % meas Qx
['BR' ring2 '.BQ-V-ST/Samples#samples'],...         % meas Qy
['BR' ring '.BPM/AcquisitionOrbit#position'],...
['BR' ring '.BPM/AcquisitionTrajectoryBBB'],...
['BR' ring '.BPM/AcquisitionTrajectoryRaw'],...
['BR' ring '.BPM/ExpertSetting'], ...
['BA' ring '.FREV-SD/Samples'], ...
 'BR.SCOPE01.CH01/Acquisition', ...
 'BR.SCOPE01.CH02/Acquisition' };

signalsToArchive = { ...
    ['BR' ring '.BQ/Acquisition#rawDataH'],...
    ['BR' ring '.BQ/Acquisition#rawDataV'],...
    ['BR' ring '.BQ/Setting#acqOffset'],...
    ['BR' ring '.BQ/Setting#acqPeriod'],...
    ['BR' ring '.BQ/Setting#nbOfTurns'],...
    ['BR' ring '.BQ/Setting#nbOfMeas'],...
    ['BR' ring '.BQ/Setting#exOffset'] };


theMonitor = matlabJapcMonitor(mySelector, ...
    signalsToMonitor, @(data,h)doSavePsbTbT(data,h),...
    'PSB R1 Tune kicker only H ,intensity 0.66 ');

global calibDone;
calibDone = false;

%myMonitor.saveDataPath = fullfile('/user/psbop/Ana/MDs/2017-12-08/RING2/RING2_withsextupoles/AC_DIPOLE279');
theMonitor.saveDataPath = fullfile(['./' daystring] );
theMonitor.saveData = false;
theMonitor.start;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%

%theMonitor.stop;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
return;


%% xeneric sampler 
clc
samplerSamples = 'PSB.BPM/AcquisitionTrajectoryBBB#position';
mySelector = 'PSB.USER.MD5';

%auxArray = matlabJapc.staticGetSignal(mySelector,samplerSamples)
%plot(auxArray,'sk-')

%%

myObject.stop
myObject.delete
clear all
